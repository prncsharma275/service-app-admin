<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Api Lvl
Route::get('/', function () {
    return view('welcome');
});
Route::get('policy', function () {
    return view('policy');
});

Route::get('apiCal',array('as'=>'apiCal', 'uses'=>'UsersController@apiCal'));
Route::get('let',array('as'=>'let', 'uses'=>'UsersController@let'));

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

Route::get('pdf/{id}','UsersController@export_pdf');

//
//
//
////Route::get('/',array('as'=>'landing',  'uses'=>'WebsiteController@landingPage'));
//Route::get('login', 'Auth\AuthController@getLogin');
//
//Route::get('/404', function () {
//    return view('sales/404');
//});
//Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

//Truck route start here(18-03-2019)
//(master module start here)

// Orders Module



Auth::routes();

//Route::get('/home','middleware' => ['auth'], 'HomeController@index');
Route::get('home',array('as'=>'home', 'middleware' => ['auth'],'uses'=>'HomeController@index'));


Route::get('support',array('as'=>'support','middleware' => ['auth'],'uses'=>'SupportController@support'));
Route::get('routeCheck',array('as'=>'routeCheck','uses'=>'CheckpostController@routeCheck'));
Route::get('changedata',array('as'=>'changedata','uses'=>'OrdersController@changedata'));
Route::get('changePassword',array('as'=>'changePassword','uses'=>'UsersController@changePassword'));
Route::get('encript',array('as'=>'encript','uses'=>'UsersController@encript'));





//admin Panel lvl
Route::get('/adminlogin',array('as'=>'home', 'middleware' => ['auth'],'uses'=>'HomeController@index'));
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Advertisement
Route::get('Advertisement',array('as'=>'Advertisement', 'middleware' => ['auth'],'uses'=>'AdvertisementController@Advertisement'));
Route::get('Add_Advertisement',array('as'=>'Add_Advertisement', 'middleware' => ['auth'],'uses'=>'AdvertisementController@Add_Advertisement'));
Route::post('store_Advertisement',array('as'=>'store_Advertisement', 'middleware' => ['auth'],'uses'=>'AdvertisementController@store_Advertisement'));
Route::get('/edit_Advertisement/{id}',array('as'=>'edit_Advertisement','uses'=>'AdvertisementController@edit_Advertisement'));
Route::post('update_Advertisement/{id}',array('as'=>'update_Advertisement','uses'=>'AdvertisementController@update_Advertisement'));
Route::get('Delete_Advertisement/{id}',array('as'=>'Delete_Advertisement','uses'=>'AdvertisementController@Delete_Advertisement'));

//Location
Route::get('Location',array('as'=>'Location', 'middleware' => ['auth'],'uses'=>'LocationController@Location'));
Route::get('Add_Location',array('as'=>'Add_Location', 'middleware' => ['auth'],'uses'=>'LocationController@Add_Location'));
Route::post('store_location',array('as'=>'store_location', 'middleware' => ['auth'],'uses'=>'LocationController@store_location'));
Route::get('/Edit_Location/{id}',array('as'=>'Edit_Location','uses'=>'LocationController@Edit_Location'));
Route::post('update_location/{id}',array('as'=>'update_location','uses'=>'LocationController@update_location'));
Route::get('Delete_Location/{id}',array('as'=>'Delete_Location','uses'=>'LocationController@Delete_Location'));

//Services
Route::get('ServicesIcons',array('as'=>'ServicesIcons', 'middleware' => ['auth'],'uses'=>'ServicesIconsController@ServicesIcons'));
Route::get('Add_ServicesIcons',array('as'=>'Add_ServicesIcons', 'middleware' => ['auth'],'uses'=>'ServicesIconsController@Add_ServicesIcons'));
Route::post('store_ServicesIcons',array('as'=>'store_ServicesIcons', 'middleware' => ['auth'],'uses'=>'ServicesIconsController@store_ServicesIcons'));
Route::get('/Edit_ServicesIcons/{id}',array('as'=>'Edit_ServicesIcons','uses'=>'ServicesIconsController@Edit_ServicesIcons'));
Route::post('update_ServicesIcons/{id}',array('as'=>'update_ServicesIcons','uses'=>'ServicesIconsController@update_ServicesIcons'));
Route::get('Delete_ServicesIcons/{id}',array('as'=>'Delete_ServicesIcons','uses'=>'ServicesIconsController@Delete_ServicesIcons'));

//Services
Route::get('Services',array('as'=>'Services', 'middleware' => ['auth'],'uses'=>'ServicesController@Services'));
Route::get('Add_Services',array('as'=>'Add_Services', 'middleware' => ['auth'],'uses'=>'ServicesController@Add_Services'));
Route::post('store_service',array('as'=>'store_service', 'middleware' => ['auth'],'uses'=>'ServicesController@store_service'));
Route::get('/Edit_Services/{id}',array('as'=>'Edit_Services','uses'=>'ServicesController@Edit_Services'));
Route::post('update_Services/{id}',array('as'=>'update_Services','uses'=>'ServicesController@update_Services'));
Route::get('Delete_Services/{id}',array('as'=>'Delete_Services','uses'=>'ServicesController@Delete_Services'));

//Worker Details
Route::get('Worker',array('as'=>'Worker', 'middleware' => ['auth'],'uses'=>'WorkerAdminController@Worker'));
Route::get('Add_Worker',array('as'=>'Add_Worker','uses'=>'WorkerAdminController@Add_Worker'));
Route::post('store_worker',array('as'=>'store_worker','uses'=>'WorkerAdminController@store_worker'));
Route::get('/Edit_Worker/{id}',array('as'=>'Edit_Worker','uses'=>'WorkerAdminController@Edit_Worker'));
Route::post('update_worker/{id}',array('as'=>'update_worker','uses'=>'WorkerAdminController@update_worker'));

//Designation
Route::get('Designation',array('as'=>'Designation', 'middleware' => ['auth'],'uses'=>'DesignationAdminController@Designation'));
Route::get('Add_Designation',array('as'=>'Add_Designation','uses'=>'DesignationAdminController@Add_Designation'));
Route::post('store_Designation',array('as'=>'store_Designation','uses'=>'DesignationAdminController@store_Designation'));
Route::get('/Edit_Designation/{id}',array('as'=>'Edit_Designation','uses'=>'DesignationAdminController@Edit_Designation'));
Route::post('update_Designation/{id}',array('as'=>'update_Designation','uses'=>'DesignationAdminController@update_Designation'));
Route::get('Delete_Designation/{id}',array('as'=>'Delete_Designation','uses'=>'DesignationAdminController@Delete_Designation'));


//package
Route::get('Package',array('as'=>'Package', 'middleware' => ['auth'],'uses'=>'PackageAdminController@Package'));
Route::get('Add_Package',array('as'=>'Add_Package','uses'=>'PackageAdminController@Add_Package'));
Route::post('store_Package',array('as'=>'store_Package','uses'=>'PackageAdminController@store_Package'));
Route::get('/Edit_Package/{id}',array('as'=>'Edit_Package','uses'=>'PackageAdminController@Edit_Package'));
Route::post('update_Package/{id}',array('as'=>'update_Package','uses'=>'PackageAdminController@update_Package'));
Route::get('Delete_Package/{id}',array('as'=>'Delete_Package','uses'=>'PackageAdminController@Delete_Package'));


//booking
Route::get('Booking',array('as'=>'Booking', 'middleware' => ['auth'],'uses'=>'BookingAdminController@Booking'));
Route::get('/Edit_Booking/{id}',array('as'=>'Edit_Booking','uses'=>'BookingAdminController@Edit_Booking'));
Route::post('update_Booking/{id}',array('as'=>'update_Booking','uses'=>'BookingAdminController@update_Booking'));

//Support
Route::get('Support',array('as'=>'Support', 'middleware' => ['auth'],'uses'=>'SupportAdminController@Support'));
Route::get('/Edit_Support/{id}',array('as'=>'Edit_Support','uses'=>'SupportAdminController@Edit_Support'));
Route::post('update_Support/{id}',array('as'=>'update_Support','uses'=>'SupportAdminController@update_Support'));

//Version
Route::get('Version',array('as'=>'Version', 'middleware' => ['auth'],'uses'=>'UsersController@Version'));
Route::get('/Edit_Version/{id}',array('as'=>'Edit_Version','uses'=>'UsersController@Edit_Version'));
Route::post('update_Version/{id}',array('as'=>'update_Version','uses'=>'UsersController@update_Version'));

//Most Search
Route::get('MostSearch',array('as'=>'MostSearch', 'middleware' => ['auth'],'uses'=>'MostSearchAdminController@MostSearch'));
Route::get('Add_MostSearch',array('as'=>'Add_MostSearch','uses'=>'MostSearchAdminController@Add_MostSearch'));
Route::post('store_MostSearch',array('as'=>'store_MostSearch','uses'=>'MostSearchAdminController@store_MostSearch'));
Route::get('/Edit_MostSearch/{id}',array('as'=>'Edit_MostSearch','uses'=>'MostSearchAdminController@Edit_MostSearch'));
Route::post('update_MostSearch/{id}',array('as'=>'update_MostSearch','uses'=>'MostSearchAdminController@update_MostSearch'));
Route::get('Delete_MostSearch/{id}',array('as'=>'Delete_MostSearch','uses'=>'MostSearchAdminController@Delete_MostSearch'));

//PDF
Route::get('invoice', function () {
    return view('pdf/invoice');
});