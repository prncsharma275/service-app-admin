<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//------------- user routes--------



//============================= Service Icons  routes ====================================

Route::post('getServiceList',array('as'=>'getServiceList', 'uses'=>'ServiceIconController@getServiceList'));
Route::post('getServiceCovers',array('as'=>'getServiceCovers', 'uses'=>'ServiceIconController@getServiceCovers'));
Route::post('getServicePackage',array('as'=>'getServicePackage', 'uses'=>'ServiceIconController@getServicePackage'));
Route::post('getSearchService',array('as'=>'getSearchService', 'uses'=>'ServiceIconController@getSearchService'));
Route::post('supportChild',array('as'=>'supportChild', 'uses'=>'WalletController@supportChild'));
Route::post('createSupportChild',array('as'=>'createSupportChild', 'uses'=>'WalletController@createSupportChild'));
//==============================support routes ends here==========================



//============================== HomePage APIS Routes===================================
Route::post('getBannerList',array('as'=>'getBannerList', 'uses'=>'HomePageController@getBannerList'));
Route::post('getMostSearchedList',array('as'=>'getMostSearchedList', 'uses'=>'HomePageController@getMostSearchedList'));
Route::post('getLocation',array('as'=>'getLocation', 'uses'=>'HomePageController@getLocation'));

//============================== HomePage APIS Routes Ends here  ===================================


//============================== auth  APIS Routes===================================
Route::post('login',array('as'=>'login', 'uses'=>'UsersController@login'));
Route::post('checkAddressExist',array('as'=>'checkAddressExist', 'uses'=>'UsersController@checkAddressExist'));
Route::post('getAddressList',array('as'=>'getAddressList', 'uses'=>'UsersController@getAddressList'));
Route::post('storeAddress',array('as'=>'storeAddress', 'uses'=>'UsersController@storeAddress'));
Route::post('getUserByEmail',array('as'=>'getUserByEmail', 'uses'=>'UsersController@getUserByEmail'));
Route::post('getUserByMobile',array('as'=>'getUserByMobile', 'uses'=>'UsersController@getUserByMobile'));
Route::post('sendRegistrationOtp',array('as'=>'sendRegistrationOtp', 'uses'=>'UsersController@sendRegistrationOtp'));
Route::post('storeUser',array('as'=>'storeUser', 'uses'=>'UsersController@storeUser'));
Route::post('checkForgetDetail',array('as'=>'checkForgetDetail', 'uses'=>'UsersController@checkForgetDetail'));
Route::post('sendSmsForPass',array('as'=>'sendSmsForPass', 'uses'=>'UsersController@sendSmsForPass'));
Route::post('getUserById',array('as'=>'getUserById', 'uses'=>'UsersController@getUserById'));
Route::post('changeName',array('as'=>'changeName', 'uses'=>'UsersController@changeName'));
Route::post('changePassword',array('as'=>'changePassword', 'uses'=>'UsersController@changePassword'));


//============================== HomePage APIS Routes Ends here  ===================================


//=============================== Booking Details ================================

Route::post('booking',array('as'=>'booking', 'uses'=>'ServiceIconController@booking'));
Route::post('bookingHistory',array('as'=>'bookingHistory', 'uses'=>'ServiceIconController@bookingHistory'));
Route::post('changeBookingDate',array('as'=>'changeBookingDate', 'uses'=>'ServiceIconController@changeBookingDate'));
Route::post('cancelBooking',array('as'=>'cancelBooking', 'uses'=>'ServiceIconController@cancelBooking'));

//============================== supports Details ===============================/=============================== Booking Details ================================

Route::post('createSupportClient',array('as'=>'createSupportClient', 'uses'=>'SupportsController@createSupportClient'));
Route::post('supportHistoryList',array('as'=>'supportHistoryList', 'uses'=>'SupportsController@supportHistoryList'));
Route::post('getSupportHistoryDetails',array('as'=>'getSupportHistoryDetails', 'uses'=>'SupportsController@getSupportHistoryDetails'));
//Route::post('changeBookingDate',array('as'=>'changeBookingDate', 'uses'=>'ServiceIconController@changeBookingDate'));
//Route::post('cancelBooking',array('as'=>'cancelBooking', 'uses'=>'ServiceIconController@cancelBooking'));

//============================== Booking Details ===============================




//============================== Wokers Details ===============================/=============================== Booking Details ================================

Route::post('getwokerByEmail',array('as'=>'getwokerByEmail', 'uses'=>'WokersController@getwokerByEmail'));
Route::post('getwokerByMobile',array('as'=>'getwokerByMobile', 'uses'=>'WokersController@getwokerByMobile'));
Route::post('createWoker',array('as'=>'createWoker', 'uses'=>'WokersController@createWoker'));
Route::post('WokersendSmsForPass',array('as'=>'WokersendSmsForPass', 'uses'=>'WokersController@WokersendSmsForPass'));
Route::post('Wokerlogin',array('as'=>'Wokerlogin', 'uses'=>'WokersController@Wokerlogin'));
Route::post('getWokerStatus',array('as'=>'getWokerStatus', 'uses'=>'WokersController@getWokerStatus'));
Route::post('wokerBookingList',array('as'=>'wokerBookingList', 'uses'=>'WokersController@wokerBookingList'));
Route::post('sendSmsbyUserId',array('as'=>'sendSmsbyUserId', 'uses'=>'WokersController@sendSmsbyUserId'));
Route::post('updatedBooking',array('as'=>'updatedBooking', 'uses'=>'WokersController@updatedBooking'));
Route::post('wokerchangePassword',array('as'=>'wokerchangePassword', 'uses'=>'WokersController@wokerchangePassword'));

//============================== Wokers Details ===============================


//========================== version code =============================
Route::get('checkversion',array('as'=>'checkversion', 'uses'=>'UsersController@checkversion'));
//============================= version code ==========================
