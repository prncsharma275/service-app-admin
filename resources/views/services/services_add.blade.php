@extends('layouts.adminPanel')
@section('content')

    <div class="viewport-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('Services')}}">Cover Services</a></li>
                <li class="breadcrumb-item active" aria-current="page">Add</li>
            </ol>
        </nav>
    </div>
    <div class="content-viewport">
        <div class="grid"><p class="grid-header">Add New Cover Services</p>

            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="row">
                        <div class="col-md-8 mx-auto">
                            <?php echo Form::open(array('url' =>['store_service'],'onsubmit'=>'return confirm("Do you really want to submit this data?");')); ?>
                            <div class="form-group row">
                                <div class="col">
                                    <label for="location_name">Service Name</label>
                                    <div class="input-group">
                                        <select id="service_name" class="form-control" name="service_name"  required >
                                            <option value="">Select Service</option>
                                            <?php $service_names=DB::table('service_icons_child')->get(); ?>
                                            @foreach($service_names as $service_name)
                                                <option value="{{$service_name->id}}">{{$service_name->service_title}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <label for="city_name">Cover Service Name</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="cover_service_name" name="cover_service_name" value="" placeholder="Cover Service Name">
                                    </div>
                                </div>
                            </div>
                            <div class="item-wrapper">
                                <div class="demo-wrapper">
                                    <button type="submit" class="btn btn-sm btn-outline-primary">Save</button>
                                    <button type="reset" class="btn btn-sm btn-outline-danger">Reset</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection