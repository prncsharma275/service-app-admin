
<!DOCTYPE html><html lang="en">
<!-- Mirrored from uxcandy.co/demo/ripple/preview/demo_1/pages/sample-pages/login_3.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 08 Jul 2019 07:53:28 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <title>Woker Admin</title>
    <!-- plugins:css --><link rel="stylesheet" href="{{ asset('assets/vendors/iconfonts/mdi/css/materialdesignicons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/css/vendor.addons.css') }}">
    <!-- endinject --><!-- vendor css for this page --><!-- End vendor css for this page --><!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('assets/css/shared/style.css') }}">
    <!-- endinject --><!-- Layout style -->
    <link rel="stylesheet" href="{{ asset('assets/css/demo_1/style.css') }}">
    <!-- Layout style -->
    <link rel="shortcut icon" href="{{ asset('assets/favicon.png') }}">
    <style>
        input{
            height: 40px !important;
            font-size: 15px !important;
            border: 1px solid lightslategray !important;
        }
    </style>
</head>
<body>
<div class="authentication-theme auth-style_3">
    <div class="row inner-wrapper">
        <div class="col-md-5 mx-auto form-section">
            <div class="logo-section">
                <a href="{{url('login')}}" class="logo">
                    <img src="{{ asset('assets/newAdminPanelLogo.png') }}" alt="logo">
                </a>
            </div>
            <form method="POST" action="{{ route('login') }}">
                <div class="form-group input-rounded">
                    {{ csrf_field() }}
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group input-rounded">
                    <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary btn-block">Login</button>
            </form>
        </div>
    </div>
</div>
<!--page body ends --><!-- SCRIPT LOADING START FORM HERE /////////////--><!-- plugins:js -->

<!-- endinject --><!-- Vendor Js For This Page Ends-->
<!-- Vendor Js For This Page Ends-->
</body>
</html>