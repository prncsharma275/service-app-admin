@extends('layouts.adminPanel')
@section('content')

    <div class="viewport-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('Support')}}">Support</a></li>
            </ol>
        </nav>
    </div>
    <div class="content-viewport">
        <div class="grid"><p class="grid-header">Support</p>
            <?php $desig=\App\Support::find($id); ?>
            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="row">
                        <div class="col-md-12 mx-auto">
                            <?php echo Form::open(array('url' =>['update_Support',$id],'files' => true, 'enctype'=>'multipart/form-data','onsubmit'=>'return confirm("Do you really want to submit this data?");')); ?>

                            <div class="form-group row">
                                <h4>{{$desig->msg}}</h4>
                            </div>
                                <div class="form-group row">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="send_support"  name="send_support"  placeholder="Support Message">
                                    </div>
                            </div>
                            <div class="item-wrapper">
                                <div class="demo-wrapper">
                                    <button type="submit" class="btn btn-sm btn-outline-primary">Send</button>
                                    <button type="reset" class="btn btn-sm btn-outline-danger">Reset</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection