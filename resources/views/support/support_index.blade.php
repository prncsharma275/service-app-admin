@extends('layouts.adminPanel')
@section('content')
    <div class="viewport-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Support</li>
            </ol>
        </nav>
    </div>
    <div class="content-viewport">
        <div class="row">
            <div class="col-lg-12">
                <div class="grid">
                    <div class="grid-body">
                        <div class="item-wrapper">
                            <div class="table-responsive">
                                <table id="sample-data-table" class="data-table table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>Support Category</th>
                                        <th>Support Date</th>
                                        <th>Support time</th>
                                        <th>Referance No</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $support=\App\Support::where('writer','=','You')->whereNull('status')->get(); ?>
                                    @foreach($support as $booking)
                                        <?php
                                        $user=DB::table('users')->find($booking->user_id);
                                        ?>
                                        <tr>
                                            <td>{{$user->name}}</td>
                                            <td>{{$booking->support_category}}</td>
                                            <td><?php echo $booking->support_date; ?></td>
                                            <td><?php echo $booking->support_time; ?></td>
                                            <td>{{$booking->ref_no}}</td>
                                            <td> <a href="{{url('Edit_Support')}}/{{$booking->id}}" class="btn btn-sm btn-outline-primary">
                                                    Support
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection