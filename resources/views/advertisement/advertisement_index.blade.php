@extends('layouts.adminPanel')
    @section('content')
        @if(Session::has('flash_message'))
            <script>
                $(document).ready(function(){
                    showSuccessToast();
                });

            </script>
        @endif
        <div class="viewport-header">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb has-arrow">
                    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Advertisement</li>
                </ol>
            </nav>
        </div>
        <div class="content-viewport">
            <a href="{{url('Add_Advertisement')}}" class="btn btn-sm btn-outline-primary">
                Add New
            </a>
            <div class="row">
                <div class="col-lg-12">
                    <div class="grid">
                        <div class="grid-body">
                            <div class="item-wrapper">
                                <div class="table-responsive">
                                    <table id="sample-data-table" class="data-table table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Image</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $advertisements=\App\Advertisement::all(); ?>
                                        @foreach($advertisements as $advertisement)
                                        <tr>
                                            <td>{{$advertisement->card_name}}</td>
                                            <td>{{$advertisement->card_img}}</td>
                                            <td> <a href="{{url('edit_Advertisement')}}/{{$advertisement->id}}" class="btn btn-sm btn-outline-primary">
                                                    Edit
                                                </a>
                                                &nbsp;&nbsp;&nbsp;
                                                <a href="{{url('Delete_Advertisement')}}/{{$advertisement->id}}" class="btn btn-sm btn-outline-danger">
                                                    Delete
                                                </a>
                                            </td>
                                        </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    @endsection