@extends('layouts.adminPanel')
@section('content')

    <div class="viewport-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('Advertisement')}}">Advertisement</a></li>
                <li class="breadcrumb-item active" aria-current="page">New</li>
            </ol>
        </nav>
    </div>
    <div class="content-viewport">
            <div class="grid"><p class="grid-header">New Advertisement</p>

                <div class="grid-body">
                    <div class="item-wrapper">
                        <div class="row">
                            <div class="col-md-8 mx-auto">
                                <?php echo Form::open(array('url' =>['store_Advertisement'],'files' => true, 'enctype'=>'multipart/form-data','onsubmit'=>'return confirm("Do you really want to submit this data?");')); ?>

                                <div class="form-group row">
                                        <div class="col">
                                            <label for="banner_name">Advertisement Name</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="banner_name" name="banner_name"  placeholder="Advertisement Name">
                                            </div>
                                        </div>
                                    <div class="col">
                                        <label for="location_name">Service Title</label>
                                        <div class="input-group">
                                            <select id="service_category" class="form-control" name="service_category"  required >
                                                <option value="">Select Service Title</option>
                                                <?php $service_names=DB::table('service_icons_child')->get(); ?>
                                                @foreach($service_names as $service_name)
                                                    <option value="{{$service_name->id}}">{{$service_name->service_title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                        <div class="col">
                                            <label for="banner">Upload Banner</label>
                                            <div class="input-group">
                                                <input type="file" class="custom-file-input" id="banner" name="banner" placeholder="Choose Banner">
                                                <label class="custom-file-label" for="banner">Choose Banner</label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="item-wrapper">
                                        <div class="demo-wrapper">
                                            <button type="submit" class="btn btn-sm btn-outline-primary">Save</button>
                                            <button type="reset" class="btn btn-sm btn-outline-danger">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

@endsection