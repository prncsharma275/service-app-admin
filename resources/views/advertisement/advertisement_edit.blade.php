@extends('layouts.adminPanel')
@section('content')

    <div class="viewport-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('Advertisement')}}">Advertisement</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit</li>
            </ol>
        </nav>
    </div>
    <div class="content-viewport">
        <?php
        $advertisement_data=\App\Advertisement::find($id);
        ?>
            <div class="grid"><p class="grid-header">Change Advertisement</p>

                <div class="grid-body">
                    <div class="item-wrapper">
                        <div class="row">
                            <div class="col-md-8 mx-auto">
                                <?php echo Form::open(array('url' =>['update_Advertisement', $id],'files' => true, 'enctype'=>'multipart/form-data','onsubmit'=>'return confirm("Do you really want to submit this data?");')); ?>
                                <div class="form-group row">
                                        <div class="col">
                                            <label for="banner_name">Advertisement Name</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="banner_name" name="banner_name" value="{{$advertisement_data->card_name}}" placeholder="Advertisement Name">
                                            </div>
                                        </div>
                                    <div class="col">
                                        <label for="location_name">Service Title</label>
                                        <div class="input-group">
                                            <select id="service_category" class="form-control" name="service_category"  required >
                                                <option value="{{$advertisement_data->service_id}}">{{$advertisement_data->title}}</option>
                                                <?php $service_names=DB::table('service_icons_child')->get(); ?>
                                                @foreach($service_names as $service_name)
                                                    <option value="{{$service_name->id}}">{{$service_name->service_title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                        <div class="col">
                                            <label for="banner">Upload Banner</label>
                                            <div class="input-group">
                                                <input type="file" class="custom-file-input" id="banner" name="banner" placeholder="Choose Banner">
                                                <label class="custom-file-label" for="banner">Choose Banner</label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="item-wrapper">
                                        <div class="demo-wrapper">
                                            <button type="submit" class="btn btn-sm btn-outline-primary">Save</button>
                                            <button type="reset" class="btn btn-sm btn-outline-danger">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <h3>Image</h3>
                        <div id="image-holder" style="width: 500px!important;height: 500px !important;">

                        </div>
                    </div>
                </div>
            </div>
        <script>
            $(document).ready(function() {
                $("#banner").on('change', function() {
                    //Get count of selected files
                    var countFiles = $(this)[0].files.length;
                    var imgPath = $(this)[0].value;
                    var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
                    var image_holder = $("#image-holder");
                    image_holder.empty();
                    if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                        if (typeof(FileReader) != "undefined") {
                            //loop for each file selected for uploaded.
                            for (var i = 0; i < countFiles; i++)
                            {
                                var reader = new FileReader();
                                reader.onload = function(e) {
                                    $("<img />", {
                                        "src": e.target.result,
                                        "class": "img img-thumbnail"
                                    }).appendTo(image_holder);
                                }
                                image_holder.show();
                                reader.readAsDataURL($(this)[0].files[i]);
                            }
                        } else {
                            alert("This browser does not support FileReader.");
                        }
                    } else {
                        alert("Pls select only images");
                    }
                });
            });
        </script>
    </div>

@endsection