@extends('layouts.adminPanel')
@section('content')

    <div class="viewport-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('Booking')}}">Booking</a></li>
                <li class="breadcrumb-item active" aria-current="page">Assign</li>
            </ol>
        </nav>
    </div>
    <div class="content-viewport">
        <div class="grid"><p class="grid-header">Assign Worker</p>
            <?php $service_icon=\App\Booking::find($id); ?>
            <div class="grid-body">
                <div class="item-wrapper">
                    <?php echo Form::open(array('url' =>['update_Booking',$id],'files' => true, 'enctype'=>'multipart/form-data','onsubmit'=>'return confirm("Do you really want to submit this data?");')); ?>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="location_name">User Name</label>
                            <div class="input-group">
                                <?php $user=DB::table('users')->find($service_icon->user_id); ?>
                                <input type="text" class="form-control" id="worker_name" name="worker_name" value="{{$user->name}}" placeholder="Worker Name" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="location_name">Date</label>
                            <div class="input-group">
                                <input type="email" class="form-control" id="email" name="email" value="<?php echo date( 'd-m-Y', strtotime($service_icon->booking_date)) ?>" placeholder="Email" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="location_name">Time</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="mobile_no" name="mobile_no" value="{{$service_icon->booking_time}}" placeholder="Mobile No" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="location_name">Service</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="password" name="password" value="{{$service_icon->service_title}}" placeholder="Password" readonly>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="location_name">Package</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="woker_add" name="woker_add" value="{{$service_icon->package_detail}}" placeholder="Worker Address" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <?php $address=DB::table('address')->find($service_icon->address_id); ?>
                            <label for="location_name">Address</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="street_one" name="street_one" value="{{$address->street_one}} , {{$address->street_two}} {{$address->city}}" placeholder="Street One" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="location_name">Worker Designation</label>

                            <div class="input-group">
                                <select id="worker" class="form-control" name="worker">
                                    <?php $service_names=DB::table('wokers')->get(); ?>
                                    @foreach($service_names as $service_name)
                                        <option value="{{$service_name->id}}">{{$service_name->woker_name}} - {{$service_name->designation_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="item-wrapper">
                        <div class="demo-wrapper">
                            <button type="submit" class="btn btn-sm btn-outline-primary">Save</button>
                            <button type="reset" class="btn btn-sm btn-outline-danger">Reset</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $("#worker").select2();
        });
    </script>
@endsection