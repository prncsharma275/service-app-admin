@extends('layouts.adminPanel')
@section('content')
    <div class="viewport-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Booking</li>
            </ol>
        </nav>
    </div>
    <div class="content-viewport">
        <div class="row">
            <div class="col-lg-12">
                <div class="grid">
                    <div class="grid-body">
                        <div class="item-wrapper">
                            <div class="table-responsive">
                                <table id="sample-data-table" class="data-table table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>Booking Date</th>
                                        <th>Booking Time</th>
                                        <th>Service</th>
                                        <th>Package Detail</th>
                                        <th>Worker</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $bookings=\App\Booking::where('status','=','Pending')->get(); ?>
                                    @foreach($bookings as $booking)
                                        <?php
                                        $service_name=DB::table('service_icons_child')->find($booking->service_id);
                                        $user=DB::table('users')->find($booking->user_id);
                                        ?>
                                        <tr>
                                            <td>{{$user->name}}</td>
                                            <td><?php echo date( 'd-m-Y', strtotime($booking->booking_date)) ?></td>
                                            <td>{{$booking->booking_time}}</td>
                                            <td>{{$service_name->service_title}}</td>
                                            <td>{{$booking->package_detail}}</td>
                                            <td>{{$booking->woker_name}}</td>
                                            <td> <a href="{{url('Edit_Booking')}}/{{$booking->id}}" class="btn btn-sm btn-outline-primary">
                                                    Assign
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection