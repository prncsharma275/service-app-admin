@extends('layouts.adminPanel')
@section('content')
    @if(Session::has('flash_message'))
        <script>
            $(document).ready(function(){
                showLocationToast();
            });

        </script>
    @endif
    @if(Session::has('flash_message_delete'))
        <script>
            $(document).ready(function(){
                showLocationDeleteToast();
            });

        </script>
    @endif
    <div class="viewport-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Location</li>
            </ol>
        </nav>
    </div>

    <div class="content-viewport">
        <a href="{{url('Add_Location')}}" class="btn btn-sm btn-outline-primary">
            Add New
        </a>
        <div class="row">
            <div class="col-lg-12">
                <div class="grid">
                    <div class="grid-body">
                        <div class="item-wrapper">
                            <div class="table-responsive">
                                <table id="sample-data-table" class="data-table table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Location</th>
                                        <th>City</th>
                                        <th>State</th>
                                        <th style="width: 10%;">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $locations=\App\Location::all(); ?>
                                    @foreach($locations as $location)
                                        <tr>
                                            <td>{{$location->location_name}}</td>
                                            <td>{{$location->location_city}}</td>
                                            <td>{{$location->location_state}}</td>
                                            <td> <a href="{{url('Edit_Location')}}/{{$location->id}}" class="btn btn-sm btn-outline-primary">
                                                    Edit
                                                </a>
                                                &nbsp;&nbsp;&nbsp;
                                                <a href="{{url('Delete_Location')}}/{{$location->id}}" class="btn btn-sm btn-outline-danger">
                                                    Delete
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection