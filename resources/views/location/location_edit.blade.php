@extends('layouts.adminPanel')
@section('content')

    <div class="viewport-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('Advertisement')}}">Location</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit</li>
            </ol>
        </nav>
    </div>
    <div class="content-viewport">
        <div class="grid"><p class="grid-header">Edit Location</p>

            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="row">
                        <div class="col-md-12 mx-auto">
                            <?php echo Form::open(array('url' =>['update_location',$id],'onsubmit'=>'return confirm("Do you really want to submit this data?");')); ?>
                            <div class="form-group row">
                                <?php $location=\App\Location::find($id); ?>
                                <div class="col">
                                    <label for="location_name">Location Name</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="location_name" name="location_name" value="{{$location->location_name}}" autocomplete="off" placeholder="Location Name">
                                    </div>
                                </div>
                                <div class="col">
                                    <label for="city_name">City Name</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="city_name" name="city_name" value="{{$location->location_city}}" placeholder="City Name">
                                    </div>
                                </div>
                                <div class="col">
                                    <label for="state_name">State Name</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="state_name" name="state_name" value="{{$location->location_state}}" placeholder="State Name">
                                    </div>
                                </div>
                            </div>
                            <div class="item-wrapper">
                                <div class="demo-wrapper">
                                    <button type="submit" class="btn btn-sm btn-outline-primary">Save</button>
                                    <button type="reset" class="btn btn-sm btn-outline-danger">Reset</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection