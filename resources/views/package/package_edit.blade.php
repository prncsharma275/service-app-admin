@extends('layouts.adminPanel')
@section('content')

    <div class="viewport-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('Package')}}">Service Package</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit</li>
            </ol>
        </nav>
    </div>
    <div class="content-viewport">
            <div class="grid"><p class="grid-header">Edit Service Package</p>
                <?php $package=\App\Service_package::find($id); ?>
                <div class="grid-body">
                    <div class="item-wrapper">
                        <div class="row">
                            <div class="col-md-12 mx-auto">
                                <?php echo Form::open(array('url' =>['update_Package',$id],'files' => true, 'enctype'=>'multipart/form-data','onsubmit'=>'return confirm("Do you really want to submit this data?");')); ?>

                                <div class="form-group row">
                                    <div class="col">
                                        <label for="location_name">Service Category</label>
                                        <div class="input-group">
                                            <select id="service_category" class="form-control" name="service_category"  required >
                                                <option value="{{$package->service_id}}">{{$package->service_name}}</option>
                                                <?php $service_names=DB::table('service_icons_child')->get(); ?>
                                                @foreach($service_names as $service_name)
                                                    <option value="{{$service_name->id}}">{{$service_name->service_title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <label for="location_name">Package</label>
                                        <div class="input-group">
                                            <select id="package" class="form-control" name="package"  required >
                                                <option value="{{$package->package_id}}">{{$package->package_name}}</option>
                                                <option value="1">Single</option>
                                                <option value="2">Combo</option>
                                                <option value="3">Gold</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <label for="location_name">Package Details</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="package_details" name="package_details" value="{{$package->package_details}}" placeholder="Package Details" required>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <label for="location_name">Package Amount</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="package_amount" name="package_amount" value="{{$package->package_amount}}" placeholder="Package Amount" required>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="item-wrapper">
                                        <div class="demo-wrapper">
                                            <button type="submit" class="btn btn-sm btn-outline-primary">Save</button>
                                            <button type="reset" class="btn btn-sm btn-outline-danger">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

@endsection