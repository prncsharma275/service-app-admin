@extends('layouts.adminPanel')
@section('content')
    <div class="viewport-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Service Package</li>
            </ol>
        </nav>
    </div>
    <div class="content-viewport">
        <a href="{{url('Add_Package')}}" class="btn btn-sm btn-outline-primary">
            Add New
        </a>
        <div class="row">
            <div class="col-lg-12">
                <div class="grid">
                    <div class="grid-body">
                        <div class="item-wrapper">
                            <div class="table-responsive">
                                <table id="sample-data-table" class="data-table table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Package Name</th>
                                        <th>Package Details</th>
                                        <th>Service Name</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $service_names=\App\Service_package::all(); ?>
                                    @foreach($service_names as $service_name)
                                        <tr>
                                            <td>{{$service_name->package_name}}</td>
                                            <td>{{$service_name->package_details}}</td>
                                            <td>{{$service_name->service_name}}</td>
                                            <td> <a href="{{url('Edit_Package')}}/{{$service_name->id}}" class="btn btn-sm btn-outline-primary">
                                                    Edit
                                                </a>
                                                &nbsp;&nbsp;&nbsp;
                                                <a href="{{url('Delete_Package')}}/{{$service_name->id}}" class="btn btn-sm btn-outline-danger">
                                                    Delete
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection