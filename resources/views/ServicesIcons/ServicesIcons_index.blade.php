@extends('layouts.adminPanel')
@section('content')
    @if(Session::has('flash_message'))
        <script>
            $(document).ready(function(){
                showServiceToast();
            });

        </script>
    @endif
    @if(Session::has('flash_message_delete'))
        <script>
            $(document).ready(function(){
                showServiceDeleteToast();
            });

        </script>
    @endif
    <div class="viewport-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Services</li>
            </ol>
        </nav>
    </div>

    <div class="content-viewport">
        <a href="{{url('Add_ServicesIcons')}}" class="btn btn-sm btn-outline-primary">
            Add New
        </a>
        <div class="row">
            <div class="col-lg-12">
                <div class="grid">
                    <div class="grid-body">
                        <div class="item-wrapper">
                            <div class="table-responsive">
                                <table id="sample-data-table" class="data-table table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Category</th>
                                        <th>Service</th>
                                        <th>Icon</th>
                                        <th style="width: 10%;">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $services=\App\ServicesIcons::all(); ?>
                                    @foreach($services as $service)
                                        <tr>
                                            <td>{{$service->service_parent_name}}</td>
                                            <td>{{$service->service_title}}</td>
                                            @if($service->service_icon!="")
                                            <td>{{$service->service_icon}}</td>
                                            @else
                                                <td><span style="color:red;">Icon Not Available..</span></td>
                                                @endif
                                            <td> <a href="{{url('Edit_ServicesIcons')}}/{{$service->id}}" class="btn btn-sm btn-outline-primary">
                                                    Edit
                                                </a>
                                                &nbsp;&nbsp;&nbsp;
                                                <a href="{{url('Delete_ServicesIcons')}}/{{$service->id}}" class="btn btn-sm btn-outline-danger">
                                                    Delete
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection