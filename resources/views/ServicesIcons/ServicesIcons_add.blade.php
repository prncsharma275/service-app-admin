@extends('layouts.adminPanel')
@section('content')

    <div class="viewport-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('ServicesIcons')}}">Services</a></li>
                <li class="breadcrumb-item active" aria-current="page">Add</li>
            </ol>
        </nav>
    </div>
    <div class="content-viewport">
        <div class="grid"><p class="grid-header">Add New Services</p>

            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="row">
                        <div class="col-md-10 mx-auto">
                            <?php echo Form::open(array('url' =>['store_ServicesIcons'],'files' => true, 'enctype'=>'multipart/form-data','onsubmit'=>'return confirm("Do you really want to submit this data?");')); ?>
                            <div class="form-group row">
                                <div class="col">
                                    <label for="location_name">Service Category</label>
                                    <div class="input-group">
                                        <select id="service_category" class="form-control" name="service_category"  required >
                                            <option value="">Select Service Category</option>
                                            <?php $service_names=DB::table('service_icon_parent')->get(); ?>
                                            @foreach($service_names as $service_name)
                                                <option value="{{$service_name->id}}">{{$service_name->service_parent_title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <label for="city_name">Service Name</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="service_name_icon" name="service_name" value="" placeholder="Service Name" required>
                                    </div>
                                </div>
                                <div class="col">
                                    <label for="location_name">Worker Designation</label>
                                    <div class="input-group">
                                        <select id="worker_designation" class="form-control" name="worker_designation" required>
                                            <?php $service_names=DB::table('woker_desig')->get(); ?>
                                            @foreach($service_names as $service_name)
                                                <option value="{{$service_name->id}}">{{$service_name->job_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <label for="icon">Upload Icon</label>
                                    <div class="input-group">
                                        <input type="file" class="custom-file-input" id="icon" name="icon" placeholder="Choose Icon" required>
                                        <label class="custom-file-label" for="icon">Choose Icon</label>
                                    </div>

                                </div>
                            </div>
                            <div class="item-wrapper">
                                <div class="demo-wrapper">
                                    <button type="submit" class="btn btn-sm btn-outline-primary">Save</button>
                                    <button type="reset" class="btn btn-sm btn-outline-danger">Reset</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                    <br><div id="image-holder" style="width: 500px!important;height: 500px !important;">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $("#worker_designation").select2();
        });
        $(document).ready(function() {
            $("#icon").on('change', function() {
                //Get count of selected files
                var countFiles = $(this)[0].files.length;
                var imgPath = $(this)[0].value;
                var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
                var image_holder = $("#image-holder");
                image_holder.empty();
                if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                    if (typeof(FileReader) != "undefined") {
                        //loop for each file selected for uploaded.
                        for (var i = 0; i < countFiles; i++)
                        {
                            var reader = new FileReader();
                            reader.onload = function(e) {
                                $("<img />", {
                                    "src": e.target.result,
                                    "class": "img img-thumbnail"
                                }).appendTo(image_holder);
                            }
                            image_holder.show();
                            reader.readAsDataURL($(this)[0].files[i]);
                        }
                    } else {
                        alert("This browser does not support FileReader.");
                    }
                } else {
                    alert("Pls select only images");
                }
            });
        });
    </script>
@endsection