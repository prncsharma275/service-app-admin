@extends('layouts.adminPanel')
@section('content')

    <div class="viewport-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('Designation')}}">Designation</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit</li>
            </ol>
        </nav>
    </div>
    <div class="content-viewport">
        <div class="grid"><p class="grid-header">Edit Designation</p>
        <?php $desig=\App\Designation::find($id); ?>
            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="row">
                        <div class="col-md-8 mx-auto">
                            <?php echo Form::open(array('url' =>['update_Designation',$id],'files' => true, 'enctype'=>'multipart/form-data','onsubmit'=>'return confirm("Do you really want to submit this data?");')); ?>

                            <div class="form-group row">
                                <div class="col">
                                    <label for="banner_name">Designation Name</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="designation_name" value="{{$desig->job_name}}" name="designation_name"  placeholder="Designation Name">
                                    </div>
                                </div>
                            </div>
                            <div class="item-wrapper">
                                <div class="demo-wrapper">
                                    <button type="submit" class="btn btn-sm btn-outline-primary">Save</button>
                                    <button type="reset" class="btn btn-sm btn-outline-danger">Reset</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection