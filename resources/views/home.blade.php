@if(Auth::user()->role!="admin")
        <?php  return redirect()->route('logout'); ?>
    @else

@extends('layouts.adminPanel')
@section('content')

    <div class="viewport-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Overview</li>
            </ol>
        </nav>
    </div>
    <div class="content-viewport">
        <div class="row">
            <div class="col-md-12 order-md-0">
                <div class="row">
                    <div class="col-3 equel-grid">
                        <div class="grid d-flex flex-column align-items-center justify-content-center">
                            <div class="grid-body text-center">
                                <div class="profile-img img-rounded bg-inverse-primary no-avatar component-flat mx-auto mb-4">
                                    <i class="mdi mdi-basket-fill mdi-2x"></i></div>
                                <?php $total_booking=\App\Booking::where('status','!=','Cancelled')->count(); ?>
                                <h2 class="font-weight-medium"><span class="animated-count">{{$total_booking}}</span></h2>
                                <small class="text-gray d-block mt-3">Total Bookings</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-3 equel-grid">
                        <div class="grid d-flex flex-column align-items-center justify-content-center">
                            <div class="grid-body text-center">
                                <div class="profile-img img-rounded bg-inverse-success no-avatar component-flat mx-auto mb-4">
                                    <i class="mdi mdi-basket-unfill mdi-2x"></i></div>
                                <?php $pending_booking=\App\Booking::where('status','=','Pending')->count(); ?>
                                <h2 class="font-weight-medium"><span class="animated-count">{{$pending_booking}}</span></h2>
                                <small class="text-gray d-block mt-3">Pending Bookings</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-3 equel-grid">
                        <div class="grid d-flex flex-column align-items-center justify-content-center">
                            <div class="grid-body text-center">
                                <div class="profile-img img-rounded bg-inverse-warning no-avatar component-flat mx-auto mb-4">
                                    <i class="mdi mdi-fire mdi-2x"></i></div>
                                <?php $completed_booking=\App\Booking::where('status','=','Completed')->count(); ?>
                                <h2 class="font-weight-medium"><span class="animated-count">{{$completed_booking}}</span></h2>
                                <small class="text-gray d-block mt-3">Completed Bookings</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-3 equel-grid">
                        <div class="grid d-flex flex-column align-items-center justify-content-center">
                            <div class="grid-body text-center">
                                <div class="profile-img img-rounded bg-inverse-success no-avatar component-flat mx-auto mb-4">
                                    <i class="mdi mdi-account-plus mdi-2x"></i></div>
                                <?php $total_users=\App\User::count(); ?>
                                <h2 class="font-weight-medium"><span class="animated-count">{{$total_users}}</span></h2>
                                <small class="text-gray d-block mt-3">Total Ueser</small>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@endif