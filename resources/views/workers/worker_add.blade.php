@extends('layouts.adminPanel')
@section('content')

    <div class="viewport-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('Worker')}}">Worker</a></li>
                <li class="breadcrumb-item active" aria-current="page">Add</li>
            </ol>
        </nav>
    </div>
    <div class="content-viewport">
        <div class="grid"><p class="grid-header">Add Worker</p>
            <div class="grid-body">
                <div class="item-wrapper">
                            <?php echo Form::open(array('url' =>['store_worker'],'files' => true, 'enctype'=>'multipart/form-data','onsubmit'=>'return confirm("Do you really want to submit this data?");')); ?>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="location_name">Worker Name</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="worker_name" name="worker_name" placeholder="Worker Name">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="location_name">Email</label>
                                    <div class="input-group">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="location_name">Mobile No</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="mobile_no" name="mobile_no" placeholder="Mobile No">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="location_name">Password</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="password" name="password" placeholder="Password">
                                    </div>
                                </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="location_name">Worker Address</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="woker_add" name="woker_add"  placeholder="Worker Address">
                                        </div>
                                    </div>
                                <div class="col-md-3">
                                    <label for="location_name">Street One</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="street_one" name="street_one"  placeholder="Street One">
                                    </div>
                                </div>
                                    <div class="col-md-3">
                                    <label for="location_name">Street Two</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="street_two" name="street_two"  placeholder="Street Two">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="location_name">City</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="city" name="city" placeholder="City">
                                    </div>
                                </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="location_name">Aadhar No</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="aadhar_no" name="aadhar_no" placeholder="Aadhar No">
                                        </div>
                                    </div>
                                <div class="col-md-3">
                                    <label for="location_name">Aadhar Document</label>
                                    <div class="input-group">
                                        <input type="file" class="form-control" id="aadhar_document" name="aadhar_document" placeholder="Aadhar Document">
                                    </div>
                                </div>
                                    <div class="col-md-3">
                                    <label for="location_name">Pan No</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="pan_no" name="pan_no"  placeholder="Pan No">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="location_name">Pan Document</label>
                                    <div class="input-group">
                                        <input type="file" class="form-control" id="pan_document" name="pan_document" placeholder="Pan Document">
                                    </div>
                                </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="location_name">Voter ID</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="voter_id" name="voter_id" placeholder="Voter ID">
                                        </div>
                                    </div>
                                <div class="col-md-3">
                                    <label for="location_name">Voter ID Document</label>
                                    <div class="input-group">
                                        <input type="file" class="form-control" id="voter_id_document" name="voter_id_document" placeholder="Voter ID Document">
                                    </div>
                                </div>
                                    <div class="col-md-3">
                                    <label for="location_name">Zip</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="zip" name="zip" placeholder="Zip">
                                    </div>
                                </div>

                                </div>
                                <br>
                                <div class="row">
                                <div class="col-md-12">
                                    <label for="location_name">Work Details</label>
                                    <div class="input-group">
                                        <textarea class="form-control" id="workDetails" name="workDetails" placeholder="Work Details"></textarea>
                                    </div>
                                </div>

                                </div>

                                <br>
                                <br>
                                <div class="row">
                                <div class="col-md-3">
                                    <label for="location_name">Worker Designation</label>
                                    <div class="input-group">
                                        <select id="worker_designation" class="form-control" name="worker_designation">
                                            <?php $service_names=DB::table('woker_desig')->get(); ?>
                                            @foreach($service_names as $service_name)
                                                    <option value="{{$service_name->id}}">{{$service_name->job_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="location_name">Status</label>
                                    <div class="input-group">
                                        <select id="status" class="form-control" name="status">
                                                <option value="Pending">Pending</option>
                                                <option value="Completed">Completed</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="location_name">Block Worker</label>
                                    <div class="input-group">
                                        <select id="block" class="form-control" name="block">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                                <br>
                            <div class="item-wrapper">
                                <div class="demo-wrapper">
                                    <button type="submit" class="btn btn-sm btn-outline-primary">Save</button>
                                    <button type="reset" class="btn btn-sm btn-outline-danger">Reset</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>

                </div>
    </div>
    <script>
        $(document).ready(function() {
            $("#worker_designation").select2();
        });
    </script>
@endsection