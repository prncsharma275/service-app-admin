<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Service_details extends Authenticatable
{
    use Notifiable;

    protected $table = 'service_details';

    protected $fillable=array('id',);

}
