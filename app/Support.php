<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Support extends Authenticatable
{
    use Notifiable;

    protected $table = 'supports';

    protected $fillable=array('id',);

}
