<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Address extends Authenticatable
{
    use Notifiable;

    protected $table = 'address';

    protected $fillable=array('id',);

}
