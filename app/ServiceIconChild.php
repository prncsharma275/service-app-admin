<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ServiceIconChild extends Authenticatable
{
    use Notifiable;

    protected $table = 'service_icons_child';

    protected $fillable=array('id',);

}
