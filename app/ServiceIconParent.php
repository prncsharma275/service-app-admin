<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ServiceIconParent extends Authenticatable
{
    use Notifiable;

    protected $table = 'service_icon_parent';

    protected $fillable=array('id',);

}
