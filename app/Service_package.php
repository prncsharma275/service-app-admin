<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Service_package extends Authenticatable
{
    use Notifiable;

    protected $table = 'service_packages';

    protected $fillable=array('id',);

}
