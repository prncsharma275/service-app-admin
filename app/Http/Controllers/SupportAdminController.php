<?php

namespace App\Http\Controllers;

use App\Support;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SupportAdminController extends Controller
{
    public function Support()
    {
        return view('support/support_index');
    }
    public function Edit_Support($id)
    {
        return view('support/support_edit')->with('id',$id);
    }
    public function update_Support(Request $request,$id)
    {
        $support=Support::find($id);
        $support_msg=new Support();
        $support_msg->support_no=$id;
        $support_msg->user_id=$support->user_id;
        $support_msg->support_category=$support->support_category;;
        $support_msg->ref_no=$support->ref_no;
        $support_msg->writer='Admin';
        $support_msg->msg=$request->send_support;
        $support_msg->support_date=date("d, M Y");
        $support_msg->read="no";
        $support_msg->support_time=date("h:i a");
        $support_msg->save();
        $support->status="Completed";
        $support->save();


        Session::flash('flash_message', 'New Support Successfully Added!');

        return redirect()->route('Support');

    }
}
