<?php

namespace App\Http\Controllers;

use App\Service_package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PackageAdminController extends Controller
{
    public function Package()
    {
        return view('package/package_index');
    }
    public function Add_Package()
    {
        return view('package/package_add');
    }
    public function Edit_Package($id)
    {
        return view('package/package_edit')->with('id',$id);
    }
    public function store_Package(Request $request)
    {
        $designation=new Service_package();
        $designation->service_id = $request->service_category;
        $service_names=DB::table('service_icons_child')->find($request->service_category);
        $designation->service_name = $service_names->service_title;
        if($request->package==1){
            $designation->package_name = "Single";
        }elseif($request->package==2){
            $designation->package_name = "Combo";
        }elseif($request->package==3){
            $designation->package_name = "Gold";
        }
        $designation->package_details = $request->package_details;
        $designation->package_amount = $request->package_amount;
        $designation->package_id = $request->package;
        $designation->save();


        Session::flash('flash_message', 'New Package Successfully Added!');

        return redirect()->route('Package');

    }
    public function update_Package(Request $request,$id)
    {
        $designation=Service_package::find($id);
        $designation->service_id = $request->service_category;
        $service_names=DB::table('service_icons_child')->find($request->service_category);
        $designation->service_name = $service_names->service_title;
        if($request->package==1){
            $designation->package_name = "Single";
        }elseif($request->package==2){
            $designation->package_name = "Combo";
        }elseif($request->package==3){
            $designation->package_name = "Gold";
        }
        $designation->package_details = $request->package_details;
        $designation->package_amount = $request->package_amount;
        $designation->package_id = $request->package;
        $designation->save();


        Session::flash('flash_message', 'New Package Successfully Added!');

        return redirect()->route('Package');

    }
    public function Delete_Package($id)
    {
        $advertisement= Service_package::find($id);
        $advertisement->delete();

        Session::flash('flash_message_delete', 'Package Record Successfully Deleted!');

        return redirect()->route('Package');
    }
}
