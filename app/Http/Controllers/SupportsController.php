<?php

namespace App\Http\Controllers;

use App\Address;
use App\Beneficery;
use App\RechKey;
use App\Order;
use App\Refferal;
use App\Support;
use App\Version;
use App\Voucher;
use App\Wallet;
use App\Callback;
use App\Wallet_add;
use App\Wallet_minus;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Http\Controllers\Auth;
use \PDF;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class SupportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function createSupportClient(Request $request)
    {
//     return Input::all();
        $supportNo = Support::count();

//        $error->time=date("h:i a");
//        $error->mydate = date("d, M Y");

        $data = new Support();
        $data->support_no = $supportNo+1;
        $data->user_id = $request->user_id;
        $data->support_category = $request->subject;
        $data->ref_no = $request->refNo;
        $data->writer = "You";
        $data->msg = $request->message;
        $data->read = "no";
        $data->support_date = date("d, M Y");
        $data->support_time = date("h:i a");
        $data->save();

        $check = Support::find($data->id);
        if (!empty($check)) {

            $back = array(
                "status" => '1',
            );
            return $back;
        } else {
            $back = array(
                "status" => '0',
            );
            return $back;
        }
    }

// ------------------------------------ Support entery ends here -----

public function supportHistoryList(Request $request){
    $data = Support::where('user_id','=',$request->user_id)->where('writer','=','You')->get();
    return $data;
}


    public function getSupportHistoryDetails(Request $request){
        $data = Support::where('user_id','=',$request->user_id)->where('support_no','=',$request->support_no)->get();
        return $data;
    }


}
