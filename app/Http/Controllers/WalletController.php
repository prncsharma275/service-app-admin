<?php

namespace App\Http\Controllers;


use App\RechKey;
use App\SupportChild;
use App\SupportMaster;
use App\Wallet_add;
use App\Wallet_minus;
use App\Wallet_refund;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use mysqli_sql_exception;
use App\Wallet;
use App\User;
use Session;

class WalletController extends Controller
{




    public  function checkPerDayAddMoneyLimit(Request $request){

//        $mydata=Input::all();
//        return $mydata;

        $todays_balance=Wallet_add::where('date',$request->date)->where('user_id',$this->decryptIt($request->user_id))->where('orderType','=','Add money')->where('amount_type','+')->sum('total_amount');
        $total_balance=$todays_balance+$request->balance;

//
        if($total_balance<=5000) {
            $data = array(
                "status" => '1',
                "msg"=>'all Ok'

            );
            return $data;
        }else{
            $data = array(
                "status" => '0',
                "msg"=>'You have exceeded per day limit of Rs.5000',
            );
            return $data;
        }
    }


    public  function checkPerDaySendToBankLimit(Request $request){

//        $mydata=Input::all();
//        return $mydata;

        $todays_balance=Wallet_minus::where('date',$request->date)->where('user_id',$this->decryptIt($request->user_id))->where('orderType','=','Send Money To Bank')->where('gatewayId','!=','Failed & Refund')->where('gatewayId','!=','FAILED')->where('gatewayId','!=','Failed')->sum('amount');
        $total_balance=$todays_balance+$request->balance;

//
        if($total_balance<=2080) {
            $data = array(
                "status" => '1',
                "msg"=>'all Ok'

            );
            return $data;
        }else{
            $data = array(
                "status" => '0',
                "msg"=>'You have exceeded per day limit of Rs.2000',
            );
            return $data;
        }
    }



    public  function checkPerDayWalletToWalletLimit(Request $request){

//        $mydata=Input::all();
//        return $mydata;

        $bank=Wallet_minus::where('date',$request->date)->where('user_id',$this->decryptIt($request->user_id))->where('orderType','=','Send Money To Bank')->where('gatewayId','!=','Failed & Refund')->where('gatewayId','!=','FAILED')->where('gatewayId','!=','Failed')->sum('amount');


        $wallet=Wallet_minus::where('date',$request->date)->where('user_id',$this->decryptIt($request->user_id))->where('orderType','=','Send Money To Another Wallet')->sum('total_amount');
        $total_balance=$bank+$wallet+$request->balance;


//
        if($total_balance<=5000) {
            $data = array(
                "status" => '1',
                "msg"=>'all Ok'

            );
            return $data;
        }else{
            $data = array(
                "status" => '0',
                "msg"=>'You have exceeded per day limit of Rs.5000',
            );
            return $data;
        }
    }

    public function razorpay(){

//        $url="https://api.razorpay.com/v1/payments/{$id}";
        $url="https://api.razorpay.com/v1/payments/pay_Ci3t4WkpgXaZ2c";
        $handle = curl_init();
        curl_setopt_array($handle,
            array(
                CURLOPT_URL => $url,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTPAUTH => CURLAUTH_ANY,
                CURLOPT_USERPWD  => "rzp_live_z5RR8SlsEXomXg:F7zwKQAWshZGAdoTuKs85X0V",
//            CURLOPT_USERPWD  => "rzp_test_rlCYCPe5A6qPEt:nx2ZybxypLsB08h5KnVzlbMG",
                CURLOPT_RETURNTRANSFER   => true,
            )
        );
        $result = curl_exec($handle);
        curl_close($handle);

        $obj = json_encode($result);
        print_r($result);
////    return $obj;
//        $json = json_decode($result);
//        return $json;

//
    }



//
    public function updateMoneyPay(Request $request){
        $item = Wallet_minus::find($request->walletid);
        $item->rechOrderId=$request->orderId;
        $item->gatewayId=$request->trasacId;
        $item->error_code=$request->error_code;
        $item->save();
        $data= Wallet::find($request->walletid);
        if($data){
            return  $data;
        }else{
            $data = array(
                "status" => "0",
            );
            return  $data;
        }
    }


    public function getBalance(Request $request){
//        $mydata=Input::all();
//        return $mydata;
//        die();
        $plusBal = Wallet_add::where('user_id','=',$this->decryptIt($request->user_id))->sum('total_amount');

        $walletRefund = Wallet_refund::where('user_id','=',$this->decryptIt($request->user_id))->sum('total_amount');

        $MinusBal = Wallet_minus::where('user_id','=',$this->decryptIt($request->user_id))->sum('total_amount');

        $balance=($plusBal+$walletRefund)-$MinusBal;
        $mybalance= sprintf('%0.2f', $balance);
        return json_encode($mybalance);
    }

    public function refBalance(Request $request){
        $plusBal = Wallet_add::where('user_id','=',$this->decryptIt($request->user_id))->where('wallet_desc','=','Refferal Bonus')->sum('total_amount');
        $mybalance= sprintf('%0.2f', $plusBal);
        return json_encode($mybalance);
    }

    public function getPassbook(Request $request){
        $data = Wallet_minus::where('user_id','=',$this->decryptIt($request->user_id))->orderBy('created_at', 'DESC')->limit(10)->get();
        return  json_encode($data);

        //     // $result = Wallet_minus::join('wallet_add', 'wallet_minus.user_id',   '=', 'wallet_add.user_id')
        //     //     ->join('wallet_refund', 'wallet_refund.id', '=', 'wallet_minus.user_id')->where('wallet_minus.user_id', $request->user_id)
        //     //     ->limit(10)->get();

        //     // return json_encode($result);

        //   $result=  DB::table('users')
        //      ->join('wallet_add', 'wallet_add.user_id', '=', 'users.id')
        //      ->join('wallet_minus', 'wallet_minus.user_id', '=', 'users.id')
        //  ->where('users.id', '=', $request->user_id)
        //  ->get();
        //   return json_encode($result);
    }







    public function addMoneyDetail(Request $request){
        $data = Wallet_add::where('user_id','=',$this->decryptIt($request->user_id))->where('amount_type','=','+')->orderBy('created_at', 'DESC')->limit(10)->get();
        return  json_encode($data);
    }


    public function lessMoneyDetail(Request $request){
        $data = Wallet_minus::where('user_id','=',$this->decryptIt($request->user_id))->where('orderType','=','Send Money To Another Wallet')->orderBy('created_at', 'DESC')->limit(10)->get();
        return  json_encode($data);
    }







    public function addmoney_panel()
    {
        $addmoneys= Wallet::where('orderType','Add money')->orderBy('id')->get();
        return View::make('addmoney/addmoney_panel')->with('area_addmoney',$addmoneys);
    }

    public function view_addmoney($id)
    {
        $viewaddmoney=Wallet::find($id);
        return View::make('addmoney/view_addmoney')->with('viewaddmoney',$viewaddmoney);
    }

    public function moneyTransfer_panel()
    {

        $moneyTransfers = Wallet::orderBy('id')
            ->where(function ($query) {
                $query->where('orderType', '=','Send Money To Another Wallet')
                    ->orWhere('orderType', '=', 'Send Money To Bank');
            })->get();


        return View::make('moneyTransfer/moneyTransfer_panel')->with('area_moneyTransfer',$moneyTransfers);
    }

    public function view_moneyTransfer($id)
    {
        $viewmoneyTransfers=Wallet::find($id);
        return View::make('moneyTransfer/view_moneyTransfer')->with('viewmoneyTransfers',$viewmoneyTransfers);
    }

    public function recharge_panel()
    {

        $recharges = Wallet::orderBy('id')
            ->where(function ($query) {
                $query->where('orderType', '=','Prepaid')
                    ->orWhere('orderType', '=', 'DTH');
            })->get();

        return View::make('recharge/recharge_panel')->with('area_recharge',$recharges);
    }


    public function view_recharge($id)
    {
        $viewrecharge=Wallet::find($id);
        return View::make('recharge/view_recharge')->with('viewrecharge',$viewrecharge);
    }





    private function encryptIt( $q ) {
        $cryptKey  = 'mussiliguri734001ecriptmusdumhainrokle';
        $qEncoded      = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $q, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
        return( $qEncoded );
    }


    private function decryptIt( $q ) {
        $cryptKey  = 'mussiliguri734001ecriptmusdumhainrokle';
        $qDecoded      = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
        return( $qDecoded );
    }

//    =================================support Master ==============================


    public function createSupport(Request $request){

//       return Input::all();

        $userId=$this->decryptIt($request->user_id);
        $count = SupportMaster::count();

        $createSu=new SupportMaster();
        $createSu->support_token=$count+1;
        $createSu->user_id=$userId;
        $createSu->status='Pending';
        $createSu->subject=$request->subject;
        $createSu->save();


        $child=new SupportChild();
        $child->support_id=$createSu->id;
        $child->message=$request->message;
        $child->mydate=date("d, M Y");
        $child->mytime=date("h:i a");
        $child->notification='on';
        $child->type=0;
        $child->ip=$request->ip();
        $child->save();

        $childCount = SupportChild::where('support_id','=',$createSu->id)->where('notification','=','on')->count();
        if(!empty($createSu)){
            $data1 = array(
                "support_id" => $createSu->support_token,
                "user_id" => $userId,
                "totalMsg" => $childCount,
                "status" => '1',
                "child_id" =>$createSu->id
            );
        }else{
            $data1 = array(
                "status" => '0',
                "totalMsg" => $childCount,
            );
        }

        return  $data1;

    }


    public function supportMaster(Request $request){

        $userId=$this->decryptIt($request->user_id);
//     $userId=$request->user_id;
        $data = SupportMaster::where('user_id','=',$userId)->where('status','=','Pending')->first();



        if(!empty($data)){
            $childCount = SupportChild::where('support_id','=',$data->id)->where('notification','=','on')->count();

            $data1 = array(
                "support_id" => $data->support_token,
                "user_id" => $userId,
                "totalMsg" => $childCount,
                "status" => '1',
                "child_id" =>$data->id
            );
        }else{

            $data1 = array(
                "status" => '0',
                "totalMsg" =>0,
            );
        }

        return  $data1;
    }



    public function supportChild(Request $request){
        $data = SupportChild::where('support_id','=',$request->support_id)->orderBy('id','DESC')->get();
        return $data;
    }



    public function createSupportChild(Request $request){

//        return Input::all();

        $userId=$this->decryptIt($request->user_id);

        $data= SupportChild::where('support_id','=',$request->support_id)->where('notification', '=', 'on')
            ->update(['notification' => 'off']);



        $child=new SupportChild();
        $child->support_id=$request->support_id;
        $child->message=$request->message;
        $child->mydate=date("d, M Y");
        $child->mytime=date("h:i a");
        $child->notification='on';
        $child->type=0;
        $child->ip=$request->ip();
        $child->save();


        if(!empty($child)){
            $data1 = array(
                "status" => '1',
            );
            return $data1;
        }else{
            $data1 = array(
                "status" => '0',
            );
            return $data1;
        }

//        $data = SupportChild::where('support_id','=',$request->support_id)->orderBy('id','DESC')->get();
//        return $data;
    }



}
