<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LocationController extends Controller
{
    public function Location()
    {
        return view('location/location_index');
    }
    public function Add_Location()
    {
        return view('location/location_add');
    }
    public function store_location(Request $request)
    {
        $locations= new Location();
        $locations->location_name = $request->location_name;
        $locations->location_city = $request->city_name;
        $locations->location_state = $request->state_name;
        $locations->save();


        Session::flash('flash_message', 'New Location Record Successfully Created!');

        return redirect()->route('Location');
    }
    public function Edit_Location($id)
    {
        return view('location/location_edit')->with('id',$id);
    }
    public function update_location(Request $request,$id)
    {
        $locations= Location::find($id);
        $locations->location_name = $request->location_name;
        $locations->location_city = $request->city_name;
        $locations->location_state = $request->state_name;
        $locations->save();


        Session::flash('flash_message', 'Location Record Successfully Updated!');

        return redirect()->route('Location');
    }
    public function Delete_Location($id)
    {
        $locations= Location::find($id);
        $locations->delete();

        Session::flash('flash_message_delete', 'Location Record Successfully Updated!');

        return redirect()->route('Location');
    }
}
