<?php

namespace App\Http\Controllers;

use App\Address;
use App\Beneficery;
use App\Booking;
use App\RechKey;
use App\Order;
use App\Refferal;
use App\Support;
use App\Version;
use App\Voucher;
use App\Wallet;
use App\Callback;
use App\Wallet_add;
use App\Wallet_minus;
use App\Woker;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Http\Controllers\Auth;
use \PDF;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class WokersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getwokerByEmail(Request $request){
        $data = Woker::where('email','=',$request->email)->first();
        if($data){
            $data1 = array(
                "id" => $data->id,
                "name" => $data->woker_name,
                "email" => $data->email,
                "mobile" => $data->mobile,
                "block" => $data->block,
                "status"=>'1'
            );
            return json_encode($data1) ;
        }else{
            $data = array(
                "status" => "0",
            );
            return json_encode($data) ;
        }

    }


    public function getwokerByMobile(Request $request){


        $data=Woker::where('mobile','=',$request->mobile)->first();
        if($data){
            $data1 = array(
                "id" => $data->id,
                "name" => $data->woker_name,
                "email" => $data->email,
                "mobile" => $data->mobile,
                "block" => $data->block,
                "status"=>'1'
            );
            return json_encode($data1) ;
        }else{
            $data1 = array(
                "status" => "0",
            );
            return json_encode($data1) ;
        }

    }




    public function createWoker(Request $request)
    {
//    return Input::all();

        $user = new Woker();
        $user->woker_name = $request->name;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->workDetails = $request->workDetails;
        $user->password = $request->password;
        $user->checkStatus = 'Pending';
        $user->save();

//

//  ------------------ end here ------------------
        $data= Woker::find($user->id);
        if($data){
            $data1 = array(
                "id" => $data->id,
                "name" => $data->woker_name,
                "email" => $data->email,
                "mobile" => $data->mobile,
                "status" => '1',

            );
            return json_encode($data1) ;
        }else{
            $data = array(
                "status" => "0",
            );
            return $data;
        }

    }







    public function WokersendSmsForPass(Request $request){
        $data=Woker::where('id','=',$request->uid)->first();
        if($data){





            $msgKey='293298A5G1iaIOZm25d7639a5';


            $url="http://api.msg91.com/api/sendhttp.php?country=91&sender=WOKERS&route=4&mobiles={$data->mobile}&authkey={$msgKey}&encrypt=&message=Welcome To WOKER Your Email is *********** and  password is $data->password&unicode=1&response=json&campaign=";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            $result = curl_exec($ch);
            curl_close($ch);

            $obj = json_decode($result);
//
            $data1 = array(
                "status" => "1",
            );
            return json_encode($data1) ;
        }else{
            $data = array(
                "status" => "0",
            );
            return json_encode($data) ;
        }
    }





    public function Wokerlogin(Request $request){


        $data = Woker::where('email',$request->email)->orWhere('mobile',$request->email)->first();

        if($data){


            if($request->password==$data->password){

                $data1 = array(
                    "status" => "1",
                    "name" => $data->woker_name,
                    "email" => $data->email,
                    "mobile" => $data->mobile,
                    "id" => $data->id,
                    "checkStatus" => $data->checkStatus,
                );
                return $data1;

            }else{
                $data = array(
                    "status" => "0",
                );
                return $data;
            }

        }else{
            $data = array(
                "status" => "0",
            );
            return $data;
        }

    }


    public function getWokerStatus(Request $request){


        $data1 = Woker::find($request->user_id);
//        return $data1;

        $data = array(
            "status" => $data1->checkStatus,
        );
        return $data;
    }


    public function wokerBookingList(Request $request){

        $data = Booking::where('woker_id','=',$request->woker_id)->where('status','=','Pending')->get();
        return $data;
    }


     public  function sendSmsbyUserId(Request $request){
         $user = User::find($request->user_id);

         $msgKey='293298A5G1iaIOZm25d7639a5';


         $url="http://api.msg91.com/api/sendhttp.php?country=91&sender=WOKERS&route=4&mobiles={$user->mobile}&authkey={$msgKey}&encrypt=&message=Your work completion otp is  $request->message&unicode=1&response=json&campaign=";

         $ch = curl_init();
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_URL, $url);
         $result = curl_exec($ch);
         curl_close($ch);

         $obj = json_decode($result);

     }


    public  function updatedBooking(Request $request){
        $book = Booking::find($request->booking_id);
        $book->status='Completed';
        $book->save();

        $data= Booking::find($book->id);
        if($data){
            $data1 = array(
                "status" => '1',
            );
            return json_encode($data1) ;
        }else{
            $data = array(
                "status" => "0",
            );
            return $data;
        }

    }




    public function wokerchangePassword(Request $request){
        $data=Woker::find($request->id);
        if($data->password==$request->oldPassword){
            $data->password = $request->newpassword;
            $data->save();

            if($data){
                $data1 = array(
                    "status"=>'1'
                );
                return json_encode($data1) ;
            }else{
                $data1 = array(
                    "status" => "0",
                    "msg"=>'Your request cannot be process this time please try after sometimes'
                );
                return json_encode($data1) ;
            }
        }else{

            $data1 = array(
                "status"=>'2',
                "msg"=>'Incorrect Password'
            );
            return json_encode($data1) ;

        }



    }



}
