<?php

namespace App\Http\Controllers;

use App\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class BookingAdminController extends Controller
{
    public function Booking()
    {
        return view('booking/booking_index');
    }
    public function Edit_Booking($id)
    {
        return view('booking/booking_edit')->with('id',$id);
    }
    public function update_Booking(Request $request,$id)
    {
        $designation=Booking::find($id);
        $worker_name=DB::table('wokers')->find($request->worker);
        $designation->woker_id = $request->worker;
        $designation->woker_name = $worker_name->woker_name;
        $designation->save();


        Session::flash('flash_message', 'New Booking Successfully Added!');

        return redirect()->route('Booking');

    }
}
