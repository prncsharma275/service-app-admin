<?php

namespace App\Http\Controllers;

use App\ErrorLog;
use App\Fee;
use App\FeeClient;
use App\FeeVender;
use App\Myroute;
use App\RechKey;
use App\VenderDailyBalance;
use App\Version;
use App\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use mysqli_sql_exception;
use App\Wallet_add;
use App\Wallet_minus;
use App\Wallet_refund;
use App\User;
use App\Order;
use Session;

class CheckpostController extends Controller
{



    public function mycheckpost(Request $request){

//       $data= Input::all();
//        return $data;

        set_time_limit(0);
        $myuserId=$this->decryptIt($request->user_id);
//        ========================== error logs===============
        $error=new ErrorLog();
        $error->user_id=$myuserId;
        $error->error="request arrived to checkpost";
        $error->date=date('Y-m-d');
        $error->time=date("h:i a");
        $error->mydate = date("d, M Y");
        $error->save();
//        ==========================Error logs ends here==========



//
//        //=================first check app block or not==================================
        $app_block_check=Version::where('block','=','1')->count();
        if($app_block_check==1){
            $data = array(
                "status" => '0',
                "msg"=>"We are performing some security updates ,kindly try after sometime. We will be up with new modules.",
            );
            return $data;
////           ============================= check app block ends here ========================

//
        }else { // this is an else part of app block false


            //=================== check user is block or not=============================
            $block_user = User::where('id', '=', $myuserId)->where('block', '=', '1')->count();
            if ($block_user == 1) {

                $errorUpdate=ErrorLog::find($error->id);
                $errorUpdate->error="User is blocked by Admin";
                $errorUpdate->save();

                $data = array(
                    "status" => '0',
                    "msg" => "You are not authorized to use this service.Please contact support",
                );
                return $data;
                // ============================= check user block ends here ========================


//
            }else{//this is an else part of user block false

                //===================================== check token =========================
                $key=RechKey::find(1);
                $code=md5($key->personalKey.$key->rechKey.$key->musid.$request->musWalletId);

                if ($request->mycode != $code) {

                    $errorUpdate=ErrorLog::find($error->id);
                    $errorUpdate->error="Security Code did not matched";
                    $errorUpdate->save();

                    $data = array(
                        "status" => '0',
                        "msg"=>"server Issue Please try again later",
                    );
                    return $data;
                    // ============================= check token ends here ========================


//
//
                }else{ // this is an else part of token check false
//
//                       //==================== sending to route by data nature============
//
                    $myroute=Myroute::where('send_route','=',$request->myroute)->first();



                    if(!empty($myroute)){

                        if($myroute->route_name=='walletRecharge'){
                            $userData=User::find($myuserId);

                            if($userData->kyc!='1'){

                                $errorUpdate=ErrorLog::find($error->id);
                                $errorUpdate->error="KYC is not done when entering walletRecharge";
                                $errorUpdate->save();

                                $data = array(
                                    "status" => '0',
                                    "msg"=>"Update Your KYC first",
                                );
                                return $data;
                            }else{
                                $getResponse1=$this->walletRecharge(
                                    $request->amount,
                                    $request->fee,
                                    $request->totalAmount,
                                    $request->wallet_desc,
                                    $request->musWalletId,
                                    $request->gatewayId,
                                    $myuserId,
                                    $request->orderType,
                                    $request->ip(),
                                    $key->rechKey,
                                    $key->musid,
                                    $request->rechargeMobileNo,
                                    $request->operatorId,
                                    $request->rechargeOrderType,
                                    $request->rechargeDescription,
                                    $request->stateCode,
                                    $request->stateName,
                                    $request->operator_name,
                                    $error->id
                                );
                                return $getResponse1;
                            }

                        }elseif($myroute->route_name=='walletToWallet'){
                            $userData=User::find($myuserId);

                            if($userData->kyc!='1'){

                                $errorUpdate=ErrorLog::find($error->id);
                                $errorUpdate->error="KYC is not done while entering to walletToWallet route";
                                $errorUpdate->save();

                                $data = array(
                                    "status" => '0',
                                    "msg"=>"Update Your KYC first",
                                );
                                return $data;
                            }else{
                                $getResponse1=$this->walletToWallet(
                                    $request->amount,
                                    $request->fee,
                                    $request->totalAmount,
                                    $request->wallet_desc,
                                    $request->musWalletId,
                                    $request->gatewayId,
                                    $myuserId,
                                    $request->orderType,
                                    $request->ip(),
                                    $key->rechKey,
                                    $key->musid,
                                    $request->rechargeMobileNo,
                                    $request->operatorId,
                                    $request->rechargeOrderType,
                                    $request->rechargeDescription,
                                    $request->stateCode,
                                    $request->stateName,
                                    $request->operator_name,
                                    $request->trackMobile,
                                    $request->moneyPayableId,
                                    $error->id
                                );
                                return $getResponse1;
                            }
                        }elseif($myroute->route_name=='addMoney'){
                            $userData=User::find($myuserId);



                            if($userData->kyc!='1'){

                                $errorUpdate=ErrorLog::find($error->id);
                                $errorUpdate->error="KYC is not done while entering to addMoney route";
                                $errorUpdate->save();


                                $data = array(
                                    "status" => '0',
                                    "msg"=>"Update Your KYC first",
                                );
                                return $data;
                            }else{
                                $getResponse1=$this->addMoney(
                                    $request->amount,
                                    $request->fee,
                                    $request->totalAmount,
                                    $request->wallet_desc,
                                    $request->musWalletId,
                                    $request->gatewayId,
                                    $myuserId,
                                    $request->orderType,
                                    $request->ip(),
                                    $key->rechKey,
                                    $key->musid,
                                    $request->rechargeMobileNo,
                                    $request->operatorId,
                                    $request->rechargeOrderType,
                                    $request->rechargeDescription,
                                    $request->stateCode,
                                    $request->stateName,
                                    $request->operator_name,
                                    $error->id
                                );
                                return $getResponse1;


                            }

                        }elseif($myroute->route_name=='bankTransfer'){
                            $userData=User::find($myuserId);

                            if($userData->kyc!='1'){

                                $errorUpdate=ErrorLog::find($error->id);
                                $errorUpdate->error="KYC is not done while entering to bankTransfer route";
                                $errorUpdate->save();

                                $data = array(
                                    "status" => '0',
                                    "msg"=>"Update Your KYC first",
                                );
                                return $data;
                            }else{
                                $getResponse1=$this->bankTransfer(
                                    $request->amount,
                                    $request->fee,
                                    $request->totalAmount,
                                    $request->wallet_desc,
                                    $request->musWalletId,
                                    $request->gatewayId,
                                    $myuserId,
                                    $request->orderType,
                                    $request->ip(),
                                    $key->rechKey,
                                    $request->transferCustomerMobile,
                                    $request->transferAmount,
                                    $request->beneficeryId,
                                    $error->id
                                );
                                return $getResponse1;

                            }


                        }elseif($myroute->route_name=='busWalletRecharge'){

                            $getResponse1=$this->busWalletRecharge();
                            return $getResponse1;



                        }elseif($myroute->route_name=='voucherWalletRecharge'){

                            $getResponse1=$this->voucherWalletRecharge();
                            return $getResponse1;



                        }elseif($myroute->route_name=='gatewayRecharge'){

                            $getResponse1=$this->gatewayRecharge(
                                $request->amount,
                                $request->fee,
                                $request->totalAmount,
                                $request->wallet_desc,
                                $request->musWalletId,
                                $request->gatewayId,
                                $myuserId,
                                $request->orderType,
                                $request->ip(),
                                $key->rechKey,
                                $key->musid,
                                $request->rechargeMobileNo,
                                $request->operatorId,
                                $request->rechargeOrderType,
                                $request->rechargeDescription,
                                $request->stateCode,
                                $request->stateName,
                                $request->operator_name,
                                $error->id
                            );
                            return $getResponse1;



                        }elseif($myroute->route_name=='voucherGatewayRecharge'){

                            $getResponse1=$this->voucherGatewayRecharge();
                            return $getResponse1;



                        }elseif($myroute->route_name=='busGatewayRecharge'){

                            $getResponse1=$this->busGatewayRecharge();
                            return $getResponse1;



                        }else{

                            $errorUpdate=ErrorLog::find($error->id);
                            $errorUpdate->error="Trying to enter unknown route";
                            $errorUpdate->save();


                            $data = array(
                                "status" => '0',
                                "msg"=>"server Issue Please try again later",
                            );
                            return $data;
                        }


                    }else{

                        $errorUpdate=ErrorLog::find($error->id);
                        $errorUpdate->error="Trying to enter unknown route";
                        $errorUpdate->save();


                        $data = array(
                            "status" => '0',
                            "msg"=>"server Issue Please try again later",
                        );
                        return $data;
                    }

                }



            }




        }


    }


//=====================================Encript data==================================
    function encryptData($data, $apiToken) {

        $method = 'aes-256-cbc';
        $password = substr(hash('sha256', $apiToken, true), 0, 32);
// IV must be exact 16 chars (128 bit)
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

        $encrypted = base64_encode(openssl_encrypt($data, $method, $password, OPENSSL_RAW_DATA, $iv));
        return $encrypted;

    }
    //=====================================encript data ends here==============================
//====================================================== Rech api function===============================
    private function recharge($rechKey,$mobile_no,$amount,$operator_id,$systemRef,$musId){

        $time = time();
//        random key

        $url = "http://api.rechapi.com/recharge.php?format=json&secure=1&userId={$musId}";

        $rechargeData = "token={$rechKey}&mobile={$mobile_no}&amount={$amount}&opid={$operator_id}&urid={$systemRef}&time={$time}";

        $data = $this->encryptData($rechargeData, $rechKey);


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "data={$data}");

//So that curl_exec returns the contents of the cURL; rather than echoing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

//execute post
        $result = curl_exec($ch);
        return $result;

    }
//=================================================Rech api ends here=============================



//==================================================Razorpay checking api=============================
    private function razorpay($id){

        $url="https://api.razorpay.com/v1/payments/{$id}";
//    $url="https://api.razorpay.com/v1/payments/pay_CatjbHv2sgXiiz";
        $handle = curl_init();
        curl_setopt_array($handle,
            array(
                CURLOPT_URL => $url,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTPAUTH => CURLAUTH_ANY,
                CURLOPT_USERPWD  => "rzp_live_z5RR8SlsEXomXg:F7zwKQAWshZGAdoTuKs85X0V",
                // CURLOPT_USERPWD  => "rzp_test_rlCYCPe5A6qPEt:nx2ZybxypLsB08h5KnVzlbMG",
                CURLOPT_RETURNTRANSFER   => true,
            )
        );
        $result = curl_exec($handle);
        curl_close($handle);

        $obj = json_encode($result);
//    return $obj;
        $json = json_decode($result);
        return $json;

//
    }






//========================================================Razorpay api ends here===============================










//====================================================gatway recharge starts here=====================
    private function gatewayRecharge(
        $amount,
        $fee,
        $totalAmount,
        $wallet_desc,
        $musWalletId,
        $gatewayId,
        $user_id,
        $orderType,
        $clientIp,
        $rechKey,
        $rechMusId,
        $rechargeMobile,
        $operator_id,
        $rechargeOrderType,
        $rechargeDescription,
        $state_code,
        $state_name,
        $operator_name,
        $errorKey
    ){
        //================add money details to razor pay=====
        set_time_limit(0);

        $json = $this->razorpay($gatewayId);


        if (array_key_exists("id",$json))
        {

            $currentDate=date('Y-m-d');
            $gateWayDate=date('Y-m-d', $json->created_at);
            if($currentDate==$gateWayDate){

                //check gatway id already exist or not
                $wallet_id=Wallet_add::where('gatewayId','=',$gatewayId)->count();



                if($wallet_id==0){
                    $gatwayAmount=$json->amount/100;
                    $freshGatway= sprintf('%0.2f',$gatwayAmount);
                    $rechargeAmount= sprintf('%0.2f', $totalAmount);
                    if($freshGatway==$rechargeAmount) {


                        $item = new Wallet_add();
                        $item->date = date('Y-m-d');
                        $item->amount = $amount;
                        $item->fee = $fee;
                        $item->total_amount = $totalAmount;
                        $item->amount_type = '+';
                        $item->wallet_desc = $wallet_desc;
                        $item->musWalletId = $musWalletId;
                        $item->gatewayId = $gatewayId;
                        $item->trackMobile = '';
                        $item->user_id = $user_id;
                        $item->orderType = $orderType;
                        $item->mytime = date("h:i a");
                        $item->mydate = date("d, M Y");
                        $item->client_ip = $clientIp;
                        $item->save();
//                    ========================== now minus the wallet value for recharge==============

                        $minus = new Wallet_minus();
                        $minus->date = date('Y-m-d');
                        $minus->amount = $amount;
                        $minus->fee = $fee;
                        $minus->total_amount = $totalAmount;
                        $minus->amount_type = '-';
                        $minus->wallet_desc = "deduct money For recharge/payment against transaction No.".$musWalletId;
                        $minus->musWalletId = $musWalletId;
                        $minus->gatewayId = "Issued";
                        $minus->trackMobile = '';
                        $minus->user_id = $user_id;
                        $minus->orderType = "deduct money For recharge/payment";
                        $minus->mytime = date("h:i a");
                        $minus->mydate = date("d, M Y");
                        $minus->client_ip = $clientIp;
                        $minus->save();




//                      ========================== Minus code ends here =============================


                        $data = Wallet_minus::find($minus->id);
                        if ($data) {


                            //   ============now send to rech api for recharge==========

                            $recharge_data=$this->recharge($rechKey,$rechargeMobile,$amount,$operator_id,$musWalletId,$rechMusId);
//
                            $myrechargeData=json_decode($recharge_data,true);
                            $newdata=$myrechargeData['data'];
//                                print_r($newdata);
//                                print_r($newdata['status']);

                            if($newdata['status']=='FAILED'){

                                $errorUpdate=ErrorLog::find($errorKey);
                                $errorUpdate->error=$newdata['resText'];
                                $errorUpdate->save();

                                $updateWallet=Wallet_minus::find($minus->id);
                                $updateWallet->gatewayId='FAILED';
                                $updateWallet->rechOrderId=(string)$newdata['orderId'];
                                $updateWallet->server_msg=(string)$newdata['resText'];
                                $updateWallet->save();


//                                    ===================adding failed detail to order table======

                                $order= new Order();
                                $order->orderDate=date('Y-m-d');
                                $order->rechAmount=$amount;
                                $order->fee=$fee;
                                $order->totalAmount=$totalAmount;
                                $order->orderDescription=$rechargeDescription;
                                $order->musOrderId=$musWalletId;
                                $order->rechOrderId=(string)$newdata['orderId'];
                                $order->operatorOrderId='0';
                                $order->oderStatus="FAILED";
                                $order->user_id=$user_id;
                                $order->orderType=$rechargeOrderType;
                                $order->mobileNo=$rechargeMobile;
                                $order->operator_id=$operator_id;
                                $order->stateCode=$state_code;
                                $order->stateName=$state_name;
                                $order->operator_name=$operator_name;
                                $order->mytime=date("h:i a");
                                $order->mydate=date("d, M Y");
                                $order->save();

//                                    ===================ends failed detail to order table======
                                $data = array(
                                    "status" => "3",
                                    "service" => $newdata['status'],
                                    "orderId" =>(string)$newdata['orderId'],
                                    "msg" => "Failed",
                                    "resText" => 'Recharge Failed.Please try after sometime',
                                );
                                return $data;


                            }elseif($newdata['status']=='SUCCESS'){

                                $errorUpdate=ErrorLog::find($errorKey);
                                $errorUpdate->error=$newdata['resText'];
                                $errorUpdate->save();

                                $updateWallet=Wallet_minus::find($minus->id);
                                $updateWallet->gatewayId=(string)$newdata['operatorId'];
                                $updateWallet->rechOrderId=(string)$newdata['orderId'];
                                $updateWallet->operatorOrderId=(string)$newdata['operatorId'];
                                $updateWallet->server_msg=(string)$newdata['resText'];
                                $updateWallet->save();

                                $order= new Order();
                                $order->orderDate=date('Y-m-d');
                                $order->rechAmount=$amount;
                                $order->fee=$fee;
                                $order->totalAmount=$totalAmount;
                                $order->orderDescription=$rechargeDescription;
                                $order->musOrderId=$musWalletId;
                                $order->rechOrderId=(string)$newdata['orderId'];
                                $order->operatorOrderId=(string)$newdata['operatorId'];
                                $order->oderStatus="SUCCESS";
                                $order->user_id=$user_id;
                                $order->orderType=$rechargeOrderType;
                                $order->mobileNo=$rechargeMobile;
                                $order->operator_id=$operator_id;
                                $order->stateCode=$state_code;
                                $order->stateName=$state_name;
                                $order->operator_name=$operator_name;
                                $order->mytime=date("h:i a");
                                $order->mydate=date("d, M Y");
                                $order->save();

                                $data = array(
                                    "status" => "1",
                                    "service" => $newdata['status'],
                                    "orderId" =>(string)$newdata['orderId'],
                                    "operatorId" =>(string)$newdata['operatorId'],
                                    "msg" => "Success",
                                    "resText" => $newdata['resText'],
                                    "refNo"=>(string)$newdata['operatorId']
                                );
                                return $data;


                            }elseif($newdata['status']=='PENDING'){


                                $errorUpdate=ErrorLog::find($errorKey);
                                $errorUpdate->error=$newdata['resText'];
                                $errorUpdate->save();


                                $updateWallet=Wallet_minus::find($minus->id);
                                $updateWallet->gatewayId='Pending';
                                $updateWallet->rechOrderId=(string)$newdata['orderId'];
                                $updateWallet->operatorOrderId=(string)$newdata['operatorId'];
                                $updateWallet->server_msg=(string)$newdata['resText'];
                                $updateWallet->save();

                                $order= new Order();
                                $order->orderDate=date('Y-m-d');
                                $order->rechAmount=$amount;
                                $order->fee=$fee;
                                $order->totalAmount=$totalAmount;
                                $order->orderDescription=$rechargeDescription;
                                $order->musOrderId=$musWalletId;
                                $order->rechOrderId=(string)$newdata['orderId'];
                                $order->operatorOrderId=(string)$newdata['operatorId'];
                                $order->oderStatus="PENDING";
                                $order->user_id=$user_id;
                                $order->orderType=$rechargeOrderType;
                                $order->mobileNo=$rechargeMobile;
                                $order->operator_id=$operator_id;
                                $order->stateCode=$state_code;
                                $order->stateName=$state_name;
                                $order->operator_name=$operator_name;
                                $order->mytime=date("h:i a");
                                $order->mydate=date("d, M Y");
                                $order->save();

                                $data = array(
                                    "status" => "2",
                                    "service" => $newdata['status'],
                                    "orderId" =>(string)$newdata['orderId'],
                                    "operatorId" =>(string)$newdata['operatorId'],
                                    "msg" => "Pending",
                                    "resText" => $newdata['resText'],
                                    "wallet_id" => (string)$minus->id,
                                    "order_id" =>(string) $order->id,
                                    "refNo"=>'Pending'

                                );
                                return $data;

                            }else{

                                $errorUpdate=ErrorLog::find($errorKey);
                                $errorUpdate->error="error with rech api given data:gatway recharge";
                                $errorUpdate->save();

                                $data = array(
                                    "status" => "0",
                                    "msg" => "Please try again after sometime"
                                );
                                return $data;
                            }

                            //   ============end of rech api for recharge==========


                        } // if failed insert then display this
                        else {

                            $errorUpdate=ErrorLog::find($errorKey);
                            $errorUpdate->error="failed to insert data in wallet minus table:gatway recharge";
                            $errorUpdate->save();

                            $data = array(
                                "status" => "0",
                                "msg" => "Please try again after sometime" //failed insert to db
                            );
                            return $data;
                        }

                        // if key invalid den display this


                        // if both amount not matched den
                    }else{

                        $errorUpdate=ErrorLog::find($errorKey);
                        $errorUpdate->error="razorpay and recharge amount did not matched: gatway recharge";
                        $errorUpdate->save();

                        $data = array(
                            "status" => "0",
                            "msg" => 'Please try again after sometime amount not matched',
                            "amount" => 'Please try again after sometime amount not matched',
                        );
                        return $data;
                    }
                }// if count greater then 0
                else{

                    $errorUpdate=ErrorLog::find($errorKey);
                    $errorUpdate->error="razorpay id already exist in wallet table:gatway recharge";
                    $errorUpdate->save();

                    $data = array(
                        "status" => "0",
                        "msg" => 'Please try again after sometime' //already have
                    );
                    return $data;
                }

                // if razorpay found nothing then

            }else{

                $errorUpdate=ErrorLog::find($errorKey);
                $errorUpdate->error="Razorpay date did not matched with transaction date :Gatway Recharge";
                $errorUpdate->save();

                $data = array(
                    "status" => "0",
                    "msg" => 'Please try again after sometime' //gateway date and current date not matched
                );
                return $data;
            } //================= razorpay else condition ends here


        }else{

            $errorUpdate=ErrorLog::find($errorKey);
            $errorUpdate->error="Razorpay key did not matched :Gatway Recharge";
            $errorUpdate->save();

            $data = array(
                "status" => "0",
                "msg" => 'Please try again after sometime' //gateway not matched
            );
            return $data;
        } //================= razorpay else condition ends here

    }
//==============================================gatway recharge ends here ===================================




//=========================================Wallet recharge starts here =========================
    private function walletRecharge(
        $amount,
        $fee,
        $totalAmount,
        $wallet_desc,
        $musWalletId,
        $gatewayId,
        $user_id,
        $orderType,
        $clientIp,
        $rechKey,
        $rechMusId,
        $rechargeMobile,
        $operator_id,
        $rechargeOrderType,
        $rechargeDescription,
        $state_code,
        $state_name,
        $operator_name,
        $errorKey
    ){
        set_time_limit(0);
        $plusBal = Wallet_add::where('user_id','=',$user_id)->sum('total_amount');
        $refundBal = Wallet_refund::where('user_id','=',$user_id)->sum('total_amount');
        $MinusBal = Wallet_minus::where('user_id','=',$user_id)->sum('total_amount');
        $balance=($plusBal+$refundBal)-$MinusBal;
        $walletBalance=(double)$balance;

        $lessAmount=(double) $totalAmount;

        //==============check negative balance given or not
        if($lessAmount>0){


            // ================== checking wallet balance is greater then recharge amount or not=====
            if($walletBalance>=$lessAmount){


                $wallet_id=Wallet_minus::where('musWalletId','=',$musWalletId)->count();

                // ==================checking order id already exist or not===========
                if($wallet_id==0){

                    $minus = new Wallet_minus();
                    $minus->date = date('Y-m-d');
                    $minus->amount = $amount;
                    $minus->fee = $fee;
                    $minus->total_amount = $totalAmount;
                    $minus->amount_type = '-';
                    $minus->wallet_desc = $wallet_desc;
                    $minus->musWalletId = $musWalletId;
                    $minus->gatewayId = $gatewayId;
                    $minus->trackMobile = '';
                    $minus->user_id = $user_id;
                    $minus->orderType = $orderType;
                    $minus->mytime = date("h:i a");
                    $minus->mydate = date("d, M Y");
                    $minus->client_ip = $clientIp;
                    $minus->save();
//                    ==========================minus code ends here=================

                    $data = Wallet_minus::find($minus->id);
                    if ($data) {


                        //   ============now send to rech api for recharge==========

                        $recharge_data=$this->recharge($rechKey,$rechargeMobile,$amount,$operator_id,$musWalletId,$rechMusId);
//
                        $myrechargeData=json_decode($recharge_data,true);
                        $newdata=$myrechargeData['data'];

                        if($newdata['status']=='FAILED'){


                            $errorUpdate=ErrorLog::find($errorKey);
                            $errorUpdate->error=$newdata['resText'];
                            $errorUpdate->save();

                            $updateWallet=Wallet_minus::find($minus->id);
                            $updateWallet->gatewayId='FAILED';
                            $updateWallet->rechOrderId=(string)$newdata['orderId'];
                            $updateWallet->save();


//                                    ===================adding failed detail to order table======

                            $order= new Order();
                            $order->orderDate=date('Y-m-d');
                            $order->rechAmount=$amount;
                            $order->fee=$fee;
                            $order->totalAmount=$totalAmount;
                            $order->orderDescription=$rechargeDescription;
                            $order->musOrderId=$musWalletId;
                            $order->rechOrderId=(string)$newdata['orderId'];
                            $order->operatorOrderId='0';
                            $order->oderStatus="FAILED";
                            $order->user_id=$user_id;
                            $order->orderType=$rechargeOrderType;
                            $order->mobileNo=$rechargeMobile;
                            $order->operator_id=$operator_id;
                            $order->stateCode=$state_code;
                            $order->stateName=$state_name;
                            $order->operator_name=$operator_name;
                            $order->mytime=date("h:i a");
                            $order->mydate=date("d, M Y");
                            $order->save();

//                                    ===================ends failed detail to order table======
                            $data = array(
                                "status" => "3",
                                "service" => $newdata['status'],
                                "orderId" =>(string)$newdata['orderId'],
                                "msg" => "Failed",
                                "resText" => "Recharge Failed"
                            );
                            return $data;


                        }elseif($newdata['status']=='SUCCESS'){


                            $errorUpdate=ErrorLog::find($errorKey);
                            $errorUpdate->error=$newdata['resText'];
                            $errorUpdate->save();

                            $updateWallet=Wallet_minus::find($minus->id);
                            $updateWallet->gatewayId=(string)$newdata['operatorId'];
                            $updateWallet->rechOrderId=(string)$newdata['orderId'];
                            $updateWallet->operatorOrderId=(string)$newdata['operatorId'];
                            $updateWallet->save();

                            $order= new Order();
                            $order->orderDate=date('Y-m-d');
                            $order->rechAmount=$amount;
                            $order->fee=$fee;
                            $order->totalAmount=$totalAmount;
                            $order->orderDescription=$rechargeDescription;
                            $order->musOrderId=$musWalletId;
                            $order->rechOrderId=(string)$newdata['orderId'];
                            $order->operatorOrderId=(string)$newdata['operatorId'];
                            $order->oderStatus="SUCCESS";
                            $order->user_id=$user_id;
                            $order->orderType=$rechargeOrderType;
                            $order->mobileNo=$rechargeMobile;
                            $order->operator_id=$operator_id;
                            $order->stateCode=$state_code;
                            $order->stateName=$state_name;
                            $order->operator_name=$operator_name;
                            $order->mytime=date("h:i a");
                            $order->mydate=date("d, M Y");
                            $order->save();

                            $data = array(
                                "status" => "1",
                                "service" => $newdata['status'],
                                "orderId" =>(string)$newdata['orderId'],
                                "operatorId" =>(string)$newdata['operatorId'],
                                "msg" => "Success",
                                "resText" => $newdata['resText'],
                                "refNo" => (string)$newdata['operatorId'],
                            );
                            return $data;


                        }elseif($newdata['status']=='PENDING'){

                            $errorUpdate=ErrorLog::find($errorKey);
                            $errorUpdate->error=$newdata['resText'];
                            $errorUpdate->save();


                            $updateWallet=Wallet_minus::find($minus->id);
                            $updateWallet->gatewayId='Pending';
                            $updateWallet->rechOrderId=(string)$newdata['orderId'];
                            $updateWallet->operatorOrderId=(string)$newdata['operatorId'];
                            $updateWallet->save();

                            $order= new Order();
                            $order->orderDate=date('Y-m-d');
                            $order->rechAmount=$amount;
                            $order->fee=$fee;
                            $order->totalAmount=$totalAmount;
                            $order->orderDescription=$rechargeDescription;
                            $order->musOrderId=$musWalletId;
                            $order->rechOrderId=(string)$newdata['orderId'];
                            $order->operatorOrderId=(string)$newdata['operatorId'];
                            $order->oderStatus="PENDING";
                            $order->user_id=$user_id;
                            $order->orderType=$rechargeOrderType;
                            $order->mobileNo=$rechargeMobile;
                            $order->operator_id=$operator_id;
                            $order->stateCode=$state_code;
                            $order->stateName=$state_name;
                            $order->operator_name=$operator_name;
                            $order->mytime=date("h:i a");
                            $order->mydate=date("d, M Y");
                            $order->save();

                            $data = array(
                                "status" => "2",
                                "service" => $newdata['status'],
                                "orderId" =>(string)$newdata['orderId'],
                                "operatorId" =>(string)$newdata['operatorId'],
                                "msg" => "Pending",
                                "resText" => $newdata['resText'],
                                "wallet_id" => $minus->id,
                                "order_id" => $order->id,
                                "refNo" => 'Pending'
                            );
                            return $data;

                        }else{

                            $errorUpdate=ErrorLog::find($errorKey);
                            $errorUpdate->error="Issue with rech api data :wallet recharge";
                            $errorUpdate->save();

                            $data = array(
                                "status" => "0",
                                "msg" => "Please try again after sometime failed"
                            );
                            return $data;
                        }

                        //   ============end of rech api for recharge==========


                    } // if failed insert then display this
                    else {

                        $errorUpdate=ErrorLog::find($errorKey);
                        $errorUpdate->error=" Datafailed to insert into wallet minus table :wallet recharge";
                        $errorUpdate->save();

                        $data = array(
                            "status" => "0",
                            "msg" => "Please try again after sometime." //failed insert to db
                        );
                        return $data;
                    }


                }else{

                    $errorUpdate=ErrorLog::find($errorKey);
                    $errorUpdate->error="duplicate entery musID already exist :wallet recharge";
                    $errorUpdate->save();

                    $data = array(
                        "status" => "0",
                        "msg"=>'Server not Responding.Please try again later,' //! Duplicate Entry not allowed
                    );
                    return $data;
                }
                // ==================checking order id already exist or not===========



            }else{

                $errorUpdate=ErrorLog::find($errorKey);
                $errorUpdate->error="walllet balance is less than recharge amount :wallet recharge";
                $errorUpdate->save();

                $data = array(
                    "status" => "0",
                    "msg"=>'Not Enough Balance'
                );
                return $data;
            }// ================== checking wallet balance is greater then recharge amount or not=====


        }else{

            $errorUpdate=ErrorLog::find($errorKey);
            $errorUpdate->error="negative amount given by user :wallet recharge";
            $errorUpdate->save();

            $data = array(
                "status" => "0",
                "msg"=>'Not a Valid Amount'
            );
            return $data;
        }  //=========checking negative value

//
    }

//============================================ wallet recharge ends here ===========================



//==========================Add money api start here ============

    private function addMoney(
        $amount,
        $fee,
        $totalAmount,
        $wallet_desc,
        $musWalletId,
        $gatewayId,
        $user_id,
        $orderType,
        $clientIp,
        $rechKey,
        $rechMusId,
        $rechargeMobile,
        $operator_id,
        $rechargeOrderType,
        $rechargeDescription,
        $state_code,
        $state_name,
        $operator_name,
        $errorKey
    ){
        set_time_limit(0);
        $json = $this->razorpay($gatewayId);


        if (array_key_exists("id",$json)) {
            $currentDate=date('Y-m-d');
            $gateWayDate=date('Y-m-d', $json->created_at);
            if($currentDate==$gateWayDate){


                //check gatway id already exist or not
                $wallet_id = Wallet_add::where('gatewayId', '=', $gatewayId)->count();


                if ($wallet_id == 0) {
                    $gatwayAmount = $json->amount / 100;
                    $freshGatway = sprintf('%0.2f', $gatwayAmount);
                    $rechargeAmount = sprintf('%0.2f', $totalAmount);
                    if ($freshGatway == $rechargeAmount) {


                        $item = new Wallet_add();
                        $item->date = date('Y-m-d');
                        $item->amount = $amount;
                        $item->fee = $fee;
                        $item->total_amount = $totalAmount;
                        $item->amount_type = '+';
                        $item->wallet_desc = $wallet_desc;
                        $item->musWalletId = $musWalletId;
                        $item->gatewayId = $gatewayId;
                        $item->trackMobile = '';
                        $item->user_id = $user_id;
                        $item->orderType = $orderType;
                        $item->mytime = date("h:i a");
                        $item->mydate = date("d, M Y");
                        $item->client_ip = $clientIp;
                        $item->save();

                        $errorUpdate=ErrorLog::find($errorKey);
                        $errorUpdate->error="Successfully Added";
                        $errorUpdate->save();


                        $data = array(
                            "status" => "1",
                            "msg" => 'Successfully Added',
                        );
                        return $data;

                    }else{

                        $errorUpdate=ErrorLog::find($errorKey);
                        $errorUpdate->error="razorpay and transaction amount did not matched :Add Money";
                        $errorUpdate->save();
                        $data = array(
                            "status" => "0",
                            "msg" => 'Please try again after sometime', //amount not matched
                        );
                        return $data;
                    }
                }// if count greater then 0
                else{

                    $errorUpdate=ErrorLog::find($errorKey);
                    $errorUpdate->error="razorpay id alrady exist in wallet minus table :Add Money";
                    $errorUpdate->save();

                    $data = array(
                        "status" => "0",
                        "msg" => 'Please try again after sometime' //already have
                    );
                    return $data;
                }

                // if razorpay found nothing then

            }else{

                $errorUpdate=ErrorLog::find($errorKey);
                $errorUpdate->error="razorpay date not matched with transaction date :Add Money";
                $errorUpdate->save();

                $data = array(
                    "status" => "0",
                    "msg" => 'Please try again after sometime' //gatway date and current date not matched
                );
                return $data;
            }

        }else{

            $errorUpdate=ErrorLog::find($errorKey);
            $errorUpdate->error="razorpay Key did not matched :Add Money";
            $errorUpdate->save();


            $data = array(
                "status" => "0",
                "msg" => 'Please try again after sometime' //gateway not matched
            );
            return $data;
        } //================= razorpay else condition ends here
//                    ========================== now minus the wallet value for recharge==============
    }
//==============================end of add money====================




    private function walletToWallet(
        $amount,
        $fee,
        $totalAmount,
        $wallet_desc,
        $musWalletId,
        $gatewayId,
        $user_id,
        $orderType,
        $clientIp,
        $rechKey,
        $rechMusId,
        $rechargeMobile,
        $operator_id,
        $rechargeOrderType,
        $rechargeDescription,
        $state_code,
        $state_name,
        $operator_name,
        $trackMobile,
        $senderMobile,
        $errorKey

    ){
        set_time_limit(0);
        $plusBal = Wallet_add::where('user_id','=',$user_id)->sum('total_amount');
        $refundBal = Wallet_refund::where('user_id','=',$user_id)->sum('total_amount');
        $MinusBal = Wallet_minus::where('user_id','=',$user_id)->sum('total_amount');
        $balance=($plusBal+$refundBal)-$MinusBal;
        $walletBalance=(double)$balance;

        $lessAmount=(double) $totalAmount;

        //==============check negative balance given or not
        if($lessAmount>0) {


            // ================== checking wallet balance is greater then recharge amount or not=====
            if ($walletBalance >= $lessAmount) {


                $wallet_id = Wallet_minus::where('musWalletId', '=', $musWalletId)->count();

                // ==================checking order id already exist or not===========
                if ($wallet_id == 0) {

                    $minus = new Wallet_minus();
                    $minus->date = date('Y-m-d');
                    $minus->amount = $amount;
                    $minus->fee = $fee;
                    $minus->total_amount = $totalAmount;
                    $minus->amount_type = '-';
                    $minus->wallet_desc = "paid Money To Mobile No.".$trackMobile;
                    $minus->musWalletId = $musWalletId;
                    $minus->gatewayId = $gatewayId;
                    $minus->trackMobile = '';
                    $minus->user_id = $user_id;
                    $minus->orderType ="Send Money To Another Wallet";
                    $minus->mytime = date("h:i a");
                    $minus->mydate = date("d, M Y");
                    $minus->client_ip = $clientIp;
                    $minus->save();
//                    ==========================minus code ends here=================

                    $data = Wallet_minus::find($minus->id);
                    if ($data) {

                        //=============== find track mobile user id=================
                        $receiver_count=User::where('mobile','=',$trackMobile)->count();

                        if($receiver_count>0){
                            $receiver=User::where('mobile','=',$trackMobile)->first();

                            $add = new Wallet_add();
                            $add->date = date('Y-m-d');
                            $add->amount = $amount;
                            $add->fee = $fee;
                            $add->total_amount = $totalAmount;
                            $add->amount_type = '+';
                            $add->wallet_desc = "Received Money from Mobile No.".$senderMobile;
                            $add->musWalletId = $musWalletId;
                            $add->gatewayId = $gatewayId;
                            $add->trackMobile = '';
                            $add->user_id = $receiver->id;
                            $add->orderType ="Received Money from another wallet";
                            $add->mytime = date("h:i a");
                            $add->mydate = date("d, M Y");
                            $add->client_ip = $clientIp;
                            $add->save();
                            $data = array(
                                "status" => "1",
                                "msg" => "Please try again after sometime"
                            );
                            return $data;
                        }else{
                            $add = new Wallet_add();
                            $add->date = date('Y-m-d');
                            $add->amount = $amount;
                            $add->fee = $fee;
                            $add->total_amount = $totalAmount;
                            $add->amount_type = '+';
                            $add->wallet_desc = "Received Money from Mobile No.".$senderMobile;
                            $add->musWalletId = $musWalletId;
                            $add->gatewayId = $gatewayId;
                            $add->trackMobile = $trackMobile;
                            $add->user_id = '';
                            $add->orderType ="Received Money from another wallet";
                            $add->mytime = date("h:i a");
                            $add->mydate = date("d, M Y");
                            $add->client_ip = $clientIp;
                            $add->save();

                            $data = array(
                                "status" => "2",
                                "msg" => "Please try again after sometime"
                            );
                            return $data;
                        }
                        //=============== find track mobile ends user id=================

                    } else {

                        $errorUpdate=ErrorLog::find($errorKey);
                        $errorUpdate->error="data failed to insert into wallet minus table :walletToWallet";
                        $errorUpdate->save();

                        $data = array(
                            "status" => "0",
                            "msg" => "Please try again after sometime "
                        );
                        return $data;
                    }


                }else{

                    $errorUpdate=ErrorLog::find($errorKey);
                    $errorUpdate->error="musId already exist in wallet minus table :walletToWallet";
                    $errorUpdate->save();


                    $data = array(
                        "status" => "0",
                        "msg"=>'Server not Responding.Please try again later'
                    );
                    return $data;
                }
                // ==================checking order id already exist or not===========



            }else{

                $errorUpdate=ErrorLog::find($errorKey);
                $errorUpdate->error="transaction amount is less than wallet amount :walletToWallet";
                $errorUpdate->save();


                $data = array(
                    "status" => "0",
                    "msg"=>'Not Enough Balance'
                );
                return $data;
            }// ================== checking wallet balance is greater then recharge amount or not=====


        }else{

            $errorUpdate=ErrorLog::find($errorKey);
            $errorUpdate->error="Negative amount given by user :walletToWallet";
            $errorUpdate->save();


            $data = array(
                "status" => "0",
                "msg"=>'Not a Valid Amount'
            );
            return $data;
        }  //=========checking negative value


    }








    private function bankTransfer(

        $amount,
        $fee,
        $totalAmount,
        $wallet_desc,
        $musWalletId,
        $gatewayId,
        $user_id,
        $orderType,
        $clientIp,
        $rechKey,
        $transferCustomerMobile,
        $transferAmount,
        $benificeryId,
        $errorKey


    ){
        set_time_limit(0);
        $plusBal = Wallet_add::where('user_id','=',$user_id)->sum('total_amount');
        $refundBal = Wallet_refund::where('user_id','=',$user_id)->sum('total_amount');
        $MinusBal = Wallet_minus::where('user_id','=',$user_id)->sum('total_amount');
        $balance=($plusBal+$refundBal)-$MinusBal;
        $walletBalance=(double)$balance;

        $lessAmount=(double) $totalAmount;

        //==============check negative balance given or not
        if($lessAmount>0) {

//            $data = array(
//                "wallet balance" => $walletBalance,
//                "transfer balance"=>$lessAmount,
//            );
//            return $data;

            // ================== checking wallet balance is greater then recharge amount or not=====
            if ($walletBalance >= $lessAmount) {


                $wallet_id = Wallet_minus::where('musWalletId', '=', $musWalletId)->count();

                // ==================checking order id already exist or not===========
                if ($wallet_id == 0) {

                    $minus = new Wallet_minus();
                    $minus->date = date('Y-m-d');
                    $minus->amount = $amount;
                    $minus->fee = $fee;
                    $minus->total_amount = $totalAmount;
                    $minus->amount_type = '-';
                    $minus->wallet_desc = $wallet_desc;
                    $minus->musWalletId = $musWalletId;
                    $minus->gatewayId = $gatewayId;
                    $minus->trackMobile = '';
                    $minus->user_id = $user_id;
                    $minus->orderType =$orderType;
                    $minus->mytime = date("h:i a");
                    $minus->mydate = date("d, M Y");
                    $minus->client_ip = $clientIp;
                    $minus->save();
//                    ==========================minus code ends here=================

                    $data = Wallet_minus::find($minus->id);
                    if ($data) {
//                        echo "hello data comming here ";
//                        print_r($data);
//                        die();

                        $url = "http://api.rechapi.com/moneyTransfer/sendMoney.php?format=json&token={$rechKey}&customerMobile={$transferCustomerMobile}&amount={$transferAmount}&beneficiaryId={$benificeryId}&urid={$musWalletId}";
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_URL, $url);
                        $result = curl_exec($ch);
                        curl_close($ch);

                        $obj=json_decode($result,true);

                        $newdata=$obj['data'];
//                        print_r($newdata);

                        if($newdata['status']=='FAILED'){


                            $errorUpdate=ErrorLog::find($errorKey);
                            $errorUpdate->error=$newdata['resText'];
                            $errorUpdate->save();

                            $updateWallet=Wallet_minus::find($minus->id);
                            $updateWallet->gatewayId='FAILED';
                            $updateWallet->rechOrderId=(string)$newdata['orderId'];
                            $updateWallet->save();

//
                            $data = array(
                                "status" => "3",
                                "msg" => "Failed",
                                "error_code" => '3',
                                "resText" => "Transaction Failed ! your amount will be refunded soon incase of amount deduction",
                                "mytext" => $newdata['resText'],
                                "refNo" => 'Failed'
                            );
                            return $data;


                        }elseif($newdata['status']=='SUCCESS'){

                            $errorUpdate=ErrorLog::find($errorKey);
                            $errorUpdate->error=(string)$newdata['orderId'];
                            $errorUpdate->save();

                            $updateWallet=Wallet_minus::find($minus->id);
                            $updateWallet->gatewayId=(string)$newdata['transId'];
                            $updateWallet->rechOrderId=(string)$newdata['orderId'];
                            $updateWallet->operatorOrderId=(string)$newdata['transId'];
                            $updateWallet->save();


                            $data = array(
                                "status" => "1",
                                "orderId" =>(string)$newdata['orderId'],
                                "error_code" => (string)$newdata['error_code'],
                                "beneficiaryName" => $newdata['beneficiaryName'],
                                "beneficiaryAccountNumber" => $newdata['beneficiaryAccountNumber'],
                                "beneficiaryName" => $newdata['beneficiaryName'],
                                "refNo" => (string)$newdata['transId']
                            );
                            return $data;


                        }elseif($newdata['status']=='PENDING'){

                            $errorUpdate=ErrorLog::find($errorKey);
                            $errorUpdate->error=(string)$newdata['orderId'];
                            $errorUpdate->save();

                            $updateWallet=Wallet_minus::find($minus->id);
                            $updateWallet->gatewayId='Pending';
                            $updateWallet->rechOrderId=(string)$newdata['orderId'];
                            $updateWallet->operatorOrderId='';
                            $updateWallet->save();


                            $data = array(
                                "status" => "2",
                                "orderId" =>(string)$newdata['orderId'],
                                "error_code" =>(string) $newdata['error_code'],
                                "beneficiaryName" => $newdata['beneficiaryName'],
                                "beneficiaryAccountNumber" => $newdata['beneficiaryAccountNumber'],
                                "beneficiaryName" => $newdata['beneficiaryName'],
                                "wallet_id" => $minus->id,
                                "refNo" => 'Pending'
                            );
                            return $data;

                        }else{

                            $errorUpdate=ErrorLog::find($errorKey);
                            $errorUpdate->error="issue with rech api data :bank Transfer";
                            $errorUpdate->save();

                            $data = array(
                                "status" => "0",
                                "msg" => "Please try again after sometime failed"
                            );
                            return $data;
                        }


                    } else {

                        $errorUpdate=ErrorLog::find($errorKey);
                        $errorUpdate->error="Data failed to insert into wallet minus table:bank Transfer";
                        $errorUpdate->save();

                        $data = array(
                            "status" => "0",
                            "msg" => "Please try again after sometime "
                        );
                        return $data;
                    }


                }else{

                    $errorUpdate=ErrorLog::find($errorKey);
                    $errorUpdate->error="wallet amount is less than transaction amount :bank Transfer wallet amount is ".$walletBalance." and transaction amount is ".$lessAmount;
                    $errorUpdate->save();

                    $data = array(
                        "status" => "0",
                        "msg"=>'Server not Responding.Please try again later'
                    );
                    return $data;
                }
                // ==================checking order id already exist or not===========



            }else{

                $errorUpdate=ErrorLog::find($errorKey);
                $errorUpdate->error="wallet amount is less than transaction amount:bank Transfer";
                $errorUpdate->save();

                $data = array(
                    "status" => "0",
                    "msg"=>'Not Enough Balance'
                );
                return $data;
            }// ================== checking wallet balance is greater then recharge amount or not=====


        }else{

            $errorUpdate=ErrorLog::find($errorKey);
            $errorUpdate->error="negative amount given by user:bank Transfer";
            $errorUpdate->save();

            $data = array(
                "status" => "0",
                "msg"=>'Not a Valid Amount'
            );
            return $data;
        }  //=========checking negative value

    }



    private function busWalletRecharge(){

        return 'hello wrold';
    }



    private function voucherWalletRecharge(){

        return 'hello wrold';
    }



    private function voucherGatewayRecharge(){

        return 'hello wrold';
    }




    private function busGatewayRecharge(){

        return 'hello wrold';
    }







//   public function insertmydata()
//   {
////       echo "hello wrold";
////       die();
//       $mydata=md5('busGatewayRecharge');
//       $data=Myroute::find(9);
//       $data->send_route=$mydata;
//       $data->save();
//       echo 'done';
//   }


    private function encryptIt( $q ) {
        $cryptKey  = 'mussiliguri734001ecriptmusdumhainrokle';
        $qEncoded      = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $q, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
        return( $qEncoded );
    }


    private function decryptIt( $q ) {
        $cryptKey  = 'mussiliguri734001ecriptmusdumhainrokle';
        $qDecoded      = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
        return( $qDecoded );
    }



}
