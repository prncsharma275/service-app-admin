<?php

namespace App\Http\Controllers;

use App\Location;
use App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ServicesController extends Controller
{
    public function Services()
    {
        return view('services/services_index');
    }
    public function Add_Services()
    {
        return view('services/services_add');
    }
    public function store_service(Request $request)
    {
        $services= new Services();
        $services->service_id = $request->service_name;
        $service_name=DB::table('service_icons_child')->where('id','=',$request->service_name)->first();
        $services->service_name = $service_name->service_title;
        $services->cover_details = $request->cover_service_name;
        $services->save();


        Session::flash('flash_message', 'New Service Record Successfully Submitted!');

        return redirect()->route('Services');
    }
    public function Edit_Services($id)
    {
        return view('services/services_edit')->with('id',$id);
    }
    public function update_Services(Request $request,$id)
    {
        $services= Services::find($id);
        $services->service_id = $request->service_name;
        $service_name=DB::table('service_icons_child')->where('id','=',$request->service_name)->first();
        $services->service_name = $service_name->service_title;
        $services->cover_details = $request->cover_service_name;
        $services->save();


        Session::flash('flash_message', 'New Service Record Successfully Submitted!');

        return redirect()->route('Services');
    }
    public function Delete_Services($id)
    {
        $services= Services::find($id);
        $services->delete();

        Session::flash('flash_message_delete', 'Location Record Successfully Updated!');

        return redirect()->route('Services');
    }
}
