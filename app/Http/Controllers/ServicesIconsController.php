<?php

namespace App\Http\Controllers;

use App\ServicesIcons;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;


class ServicesIconsController extends Controller
{
    public function ServicesIcons()
    {
        return view('ServicesIcons/ServicesIcons_index');
    }
    public function Add_ServicesIcons()
    {
        return view('ServicesIcons/ServicesIcons_add');
    }
    public function store_ServicesIcons(Request $request)
    {
        $file = $request->icon;
        $filename = time().$file->getClientOriginalName();
        $file->move('public/uploads/images/service_icons',$filename);


        $services= new ServicesIcons();
        $services->service_title = $request->service_name;
        $services->woker_desig_id = $request->worker_designation;
        $services->service_parent_id = $request->service_category;
        $service_name=DB::table('service_icon_parent')->where('id','=',$request->service_category)->first();
        $services->service_parent_name = $service_name->service_parent_title;
        $services->service_icon = $filename;
        $services->save();


        Session::flash('flash_message', 'Service Record Successfully Submitted!');

        return redirect()->route('ServicesIcons');
    }
    public function Edit_ServicesIcons($id)
    {
        return view('ServicesIcons/ServicesIcons_edit')->with('id',$id);
    }
    public function update_ServicesIcons(Request $request,$id)
    {
        $file = $request->icon;
        if(Input::hasFile('icon')) {
            $filename = time() . $file->getClientOriginalName();
            $file->move('public/uploads/images/service_icons', $filename);


            $services = ServicesIcons::find($id);
            $services->service_title = $request->service_name;
            $services->woker_desig_id = $request->worker_designation;
            $services->service_parent_id = $request->service_category;
            $service_name = DB::table('service_icon_parent')->where('id', '=', $request->service_category)->first();
            $services->service_parent_name = $service_name->service_parent_title;
            $services->service_icon = $filename;
            $services->save();
        }else{
            $services = ServicesIcons::find($id);
            $services->service_title = $request->service_name;
            $services->woker_desig_id = $request->worker_designation;
            $services->service_parent_id = $request->service_category;
            $service_name = DB::table('service_icon_parent')->where('id', '=', $request->service_category)->first();
            $services->service_parent_name = $service_name->service_parent_title;
            $services->save();
        }

        Session::flash('flash_message', 'Service Record Successfully Submitted!');

        return redirect()->route('ServicesIcons');
    }
    public function Delete_ServicesIcons($id)
    {
        $services= ServicesIcons::find($id);
        $services->delete();

        Session::flash('flash_message_delete', 'Service Record Successfully Deleted!');

        return redirect()->route('ServicesIcons');
    }
}
