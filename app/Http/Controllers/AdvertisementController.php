<?php

namespace App\Http\Controllers;

use App\Advertisement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class AdvertisementController extends Controller
{
    public function Advertisement()
    {
        return view('advertisement/advertisement_index');
    }
    public function Add_Advertisement()
    {
        return view('advertisement/advertisement_add');
    }
    public function edit_Advertisement($id)
    {
        return view('advertisement/advertisement_edit')->with('id',$id);
    }
    public function store_Advertisement(Request $request)
    {
        $file = $request->banner;
        $filename =time().$file->getClientOriginalName();
        $file->move('public/uploads/images/ads',$filename);

        $advertisement= new Advertisement();
        $advertisement->card_name = $request->banner_name;
        $advertisement->card_img = $filename;
        $advertisement->service_id = $request->service_category;
        $service_title=DB::table('service_icons_child')->find($request->service_category);
        $advertisement->title = $service_title->service_title;
        $advertisement->img_path = $service_title->service_icon;
        $advertisement->save();


        Session::flash('flash_message', 'New Advertisement Successfully Added!');

        return redirect()->route('Advertisement');

    }
    public function update_Advertisement(Request $request, $id)
    {
        $file = $request->banner;
        if(Input::hasFile('banner')){
            $filename = time().$file->getClientOriginalName();
            $file->move('public/uploads/images/ads',$filename);

            $advertisement= Advertisement::find($id);
            $advertisement->card_name = $request->banner_name;
            $advertisement->card_img = $filename;
            $advertisement->service_id = $request->service_category;
            $service_title=DB::table('service_icons_child')->find($request->service_category);
            $advertisement->title = $service_title->service_title;
            $advertisement->img_path = $service_title->service_icon;
            $advertisement->save();
        }else{
            $advertisement= Advertisement::find($id);
            $advertisement->card_name = $request->banner_name;
            $advertisement->service_id = $request->service_category;
            $service_title=DB::table('service_icons_child')->find($request->service_category);
            $advertisement->title = $service_title->service_title;
            $advertisement->img_path = $service_title->service_icon;
            $advertisement->save();
        }



        Session::flash('flash_message', 'New Advertisement Successfully Updated!');

        return redirect()->route('Advertisement');

    }

    public function Delete_Advertisement($id)
    {
        $advertisement= Advertisement::find($id);
        $advertisement->delete();

        Session::flash('flash_message_delete', 'Advertisement Record Successfully Deleted!');

        return redirect()->route('Advertisement');
    }
}
