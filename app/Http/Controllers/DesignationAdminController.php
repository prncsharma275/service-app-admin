<?php

namespace App\Http\Controllers;

use App\Designation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DesignationAdminController extends Controller
{
    public function Designation()
    {
        return view('designation/designation_index');
    }
    public function Add_Designation()
    {
        return view('designation/designation_add');
    }
    public function Edit_Designation($id)
    {
        return view('designation/designation_edit')->with('id',$id);
    }
    public function store_Designation(Request $request)
    {
        $designation=new Designation();
        $designation->job_name = $request->designation_name;
        $designation->save();


        Session::flash('flash_message', 'New Advertisement Successfully Added!');

        return redirect()->route('Designation');

    }
    public function update_Designation(Request $request,$id)
    {
        $designation=Designation::find($id);
        $designation->job_name = $request->designation_name;
        $designation->save();


        Session::flash('flash_message', 'New Advertisement Successfully Added!');

        return redirect()->route('Designation');

    }
    public function Delete_Designation($id)
    {
        $advertisement= Designation::find($id);
        $advertisement->delete();

        Session::flash('flash_message_delete', 'Designation Record Successfully Deleted!');

        return redirect()->route('Designation');
    }
}
