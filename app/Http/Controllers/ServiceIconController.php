<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Service_details;
use App\Service_package;
use App\ServiceIconParent;
use App\ServiceIconChild;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use mysqli_sql_exception;
use App\User;
use Session;

class ServiceIconController extends Controller
{

    public function getServiceList(Request $request){

        $serviceList = ServiceIconChild::where('service_parent_id','=',$request->parentId)->get();
        return $serviceList;
    }


    public function getServiceCovers(Request $request){

        $serviceList = Service_details::where('service_id','=',$request->serviceId)->get();
        return $serviceList;
    }


    public function getServicePackage(Request $request){

        $serviceList = Service_package::where('service_id','=',$request->serviceId)->where('package_id','=',$request->pack_id)->get();
        return $serviceList;
    }





    public function getSearchService(Request $request){

        $data=ServiceIconChild::where('service_title','LIKE',"%{$request->search}%")
            ->orWhere('service_parent_name', 'LIKE',"%{$request->search}%")
            ->get();

        return json_encode($data);
    }



    private function encryptIt( $q ) {
        $cryptKey  = 'mussiliguri734001ecriptmusdumhainrokle';
        $qEncoded      = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $q, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
        return( $qEncoded );
    }


    private function decryptIt( $q ) {
        $cryptKey  = 'mussiliguri734001ecriptmusdumhainrokle';
        $qDecoded      = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
        return( $qDecoded );
    }



    //==================================================Razorpay checking api=============================




    public function booking (Request $request){

//        return Input::all();
        set_time_limit(0);

        $book = new Booking();
        $book->user_id=$request->user_id;
        $book->booking_date=date("Y-m-d", strtotime($request->booking_date));
        $book->my_date= date("d, M Y", strtotime($request->booking_date));
        $book->booking_time=$request->booking_time;
        $book->service_id=$request->service_id;
        $book->package_id=$request->package_id;
        $book->address_id=$request->address_id;
        $book->package_name=$request->package_name;
        $book->package_detail=$request->package_detail;
        $book->package_rate=$request->package_rate;
        $book->payment_type=$request->payment_type;
        $book->invoice_no=$request->invoice_no;
        $book->payment_ref_no=$request->payment_ref_no;
        $book->status='Pending';
        $book->save();

        $check = Booking::find($book->id);


        if($check){
            $data = array(
                'status'=>'1',
                'date'=>$check->my_date,
                'booking_id'=>$check->id,
                'databaseDate'=>$check->booking_date
            );
            return $data;

        }else{
            $data = array(
                'status'=>'0',
                "msg"=>'Your request not proceed this time, please try after some time'
            );
            return $data;
        }
    }


    public function bookingHistory(Request $request){

        $book = Booking::where('user_id','=',$request->user_id)->where('status','=',$request->booking_type)->get();
        return $book;
    }




    public function changeBookingDate (Request $request){

//        return Input::all();
        set_time_limit(0);

        $book = Booking::find($request->booking_id);
        $book->booking_date=date("Y-m-d", strtotime($request->booking_date));
        $book->my_date= date("d, M Y", strtotime($request->booking_date));
        $book->booking_time=$request->booking_time;
        $book->save();

        $check = Booking::find($book->id);


        if($check){
            $data = array(
                'status'=>'1',
            );
            return $data;

        }else{
            $data = array(
                'status'=>'0',
            );
            return $data;
        }
    }



    public function cancelBooking(Request $request){

//        return Input::all();
        set_time_limit(0);

        $book = Booking::find($request->booking_id);
        $book->status='Cancelled';
        $book->reason_cancel=$request->reason_cancel;
        $book->save();

        $check = Booking::find($book->id);


        if($check){
            $data = array(
                'status'=>'1',
            );
            return $data;

        }else{
            $data = array(
                'status'=>'0',
            );
            return $data;
        }
    }

}


