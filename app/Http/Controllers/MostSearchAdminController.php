<?php

namespace App\Http\Controllers;

use App\Most_searched;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class MostSearchAdminController extends Controller
{
    public function MostSearch()
    {
        return view('mostsearch/mostsearch_index');
    }
    public function Add_MostSearch()
    {
        return view('mostsearch/mostsearch_add');
    }
    public function Edit_MostSearch($id)
    {
        return view('mostsearch/mostsearch_edit')->with('id',$id);
    }
    public function store_MostSearch(Request $request)
    {
        $file = $request->banner;
        $filename =time().$file->getClientOriginalName();
        $file->move('public/uploads/images/most_searched',$filename);


        $designation=new Most_searched();
        $designation->child_service_id = $request->service_category;
        $service_names=DB::table('service_icons_child')->find($request->service_category);
        $designation->service_name = $service_names->service_title;
        $designation->parent_service_id = $service_names->service_parent_id;
        $designation->service_icon_path = $service_names->service_icon;

        $designation->title = $request->title;
        $designation->desc = $request->desc;
        $designation->img_path = $filename;
        $designation->save();


        Session::flash('flash_message', 'New MostSearch Successfully Added!');

        return redirect()->route('MostSearch');

    }
    public function update_MostSearch(Request $request,$id)
    {
        $file = $request->banner;
        if(Input::hasFile('banner')) {
            $filename = time() . $file->getClientOriginalName();
            $file->move('public/uploads/images/most_searched', $filename);


            $designation =Most_searched::find($id);
            $designation->child_service_id = $request->service_category;
            $service_names = DB::table('service_icons_child')->find($request->service_category);
            $designation->service_name = $service_names->service_title;
            $designation->parent_service_id = $service_names->service_parent_id;
            $designation->service_icon_path = $service_names->service_icon;

            $designation->title = $request->title;
            $designation->desc = $request->desc;
            $designation->img_path = $filename;
            $designation->save();
        }else{
            $designation = Most_searched::find($id);
            $designation->child_service_id = $request->service_category;
            $service_names = DB::table('service_icons_child')->find($request->service_category);
            $designation->service_name = $service_names->service_title;
            $designation->parent_service_id = $service_names->service_parent_id;
            $designation->service_icon_path = $service_names->service_icon;

            $designation->title = $request->title;
            $designation->desc = $request->desc;
            $designation->save();
        }

        Session::flash('flash_message', 'New MostSearch Successfully Added!');

        return redirect()->route('MostSearch');

    }
    public function Delete_MostSearch($id)
    {
        $advertisement= Most_searched::find($id);
        $advertisement->delete();

        Session::flash('flash_message_delete', 'MostSearch Record Successfully Deleted!');

        return redirect()->route('MostSearch');
    }
}
