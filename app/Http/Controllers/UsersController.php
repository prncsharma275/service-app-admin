<?php

namespace App\Http\Controllers;

use App\Address;
use App\Beneficery;
use App\RechKey;
use App\Order;
use App\Refferal;
use App\Version;
use App\Voucher;
use App\Wallet;
use App\Callback;
use App\Wallet_add;
use App\Wallet_minus;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Http\Controllers\Auth;
use \PDF;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function login(Request $request){


        $data = User::where('email',$request->email)->orWhere('mobile',$request->email)->first();

        if($data){


            if($request->password==$data->password_show){

                $data1 = array(
                    "status" => "1",
                    "name" => $data->name,
                    "email" => $data->email,
                    "mobile" => $data->mobile,
                    "id" => $data->id,
                );
                return $data1;

            }else{
                $data = array(
                    "status" => "0",
                );
                return $data;
            }

        }else{
            $data = array(
                "status" => "0",
            );
            return $data;
        }

    }




    public function checkAddressExist(Request $request){

        $add=Address::where('user_id','=',$request->userId)->get();

        if(!$add->isEmpty()){

            $data = array(
                'status'=>'1'
            );

        }else{
            $data = array(
                'status'=>'0'
            );
        }
        return $data;
    }




    public function getAddressList(Request $request){

        $add=Address::where('user_id','=',$request->userId)->get();
        return $add;
    }


    public function storeAddress(Request $request){


        $add=new Address();
        $add->user_id=$request->user_id;
        $add->name=$request->name;
        $add->street_one=$request->street_one;
        $add->street_two=$request->street_two;
        $add->city=$request->city;
        $add->sub_location=$request->sublocation;
        $add->address_type=$request->add_type;
        $add->save();

        if($add->id){
            $data=array(
                'status'=>'1',
                'add_id'=>$add->id
            );
        }else{
            $data=array(
                'status'=>'0',
            );
        }
        return $data;
    }




    public function getUserByEmail(Request $request){
        $data = User::where('email','=',$request->email)->first();
        if($data){
            $data1 = array(
                "id" => $data->id,
                "name" => $data->name,
                "email" => $data->email,
                "mobile" => $data->mobile,
                "block" => $data->block,
                "status"=>'1'
            );
            return json_encode($data1) ;
        }else{
            $data = array(
                "status" => "0",
            );
            return json_encode($data) ;
        }
    }



    public function getUserByMobile(Request $request){
        $data=User::where('mobile','=',$request->mobile)->first();
        if($data){
            $data1 = array(
                "id" => $data->id,
                "name" => $data->name,
                "email" => $data->email,
                "mobile" => $data->mobile,
                "block" => $data->block,
                "status"=>'1'
            );
            return json_encode($data1) ;
        }else{
            $data1 = array(
                "status" => "0",
            );
            return json_encode($data1) ;
        }
    }



    public function getUserById(Request $request){
        $data=User::find($request->id);
        if($data){
            $data1 = array(
                "id" => $data->id,
                "name" => $data->name,
                "email" => $data->email,
                "mobile" => $data->mobile,
                "block" => $data->block,
                "status"=>'1'
            );
            return json_encode($data1) ;
        }else{
            $data1 = array(
                "status" => "0",
            );
            return json_encode($data1) ;
        }
    }


    public function changeName(Request $request){
        $data=User::find($request->id);
        $data->name=$request->name;
        $data->save();
        if($data){
            $data1 = array(
                "status"=>'1'
            );
            return json_encode($data1) ;
        }else{
            $data1 = array(
                "status" => "0",
            );
            return json_encode($data1) ;
        }
    }

    public function changePassword(Request $request){
        $data=User::find($request->id);
        if($data->password_show==$request->oldPassword){

            $data->password_show = $request->newpassword;
            $data->password = Hash::make ($request->newpassword);
            $data->save();

            if($data){
                $data1 = array(
                    "status"=>'1'
                );
                return json_encode($data1) ;
            }else{
                $data1 = array(
                    "status" => "0",
                    "msg"=>'Your request cannot be process this time please try after sometimes'
                );
                return json_encode($data1) ;
            }
        }else{

            $data1 = array(
                "status"=>'2',
                "msg"=>'Incorrect Password'
            );
            return json_encode($data1) ;

        }



    }


    public function sendRegistrationOtp(Request $request){


        $msgKey='293298A5G1iaIOZm25d7639a5';


        $url="http://api.msg91.com/api/sendhttp.php?country=91&sender=WOKERS&route=4&mobiles={$request->mobile_no}&authkey={$msgKey}&encrypt=&message={$request->message}&unicode=1&response=json&campaign=";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        curl_close($ch);

        $obj = json_decode($result);
//
        $data1 = array(
            "status" => "1",
        );
        return json_encode($data1) ;

    }




    public  function generateRandomString($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ#@$%&';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }



    public function storeUser(Request $request)
    {
//    return Input::all();

        $user = new User();
        $user->name = $request->name;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->password_show = $request->password;
        $user->status ='1';
        $user->password = Hash::make ($request->password);
        $user->save();

//

//  ------------------ end here ------------------
        $data= User::find($user->id);
        if($data){
            $data1 = array(
                "id" => $data->id,
                "name" => $data->name,
                "email" => $data->email,
                "mobile" => $data->mobile,
                "status" => $data->status,

            );
            return json_encode($data1) ;
        }else{
            $data = array(
                "status" => "0",
            );
            return $data;
        }

    }



    public function checkForgetDetail(Request $request){
        $data=User::where('mobile','=',$request->mobile)->first();
        if($data){
            $data1 = array(
                "id" => $data->id,
                "mobile" => $data->mobile,
                "status" => '1',
            );
            return json_encode($data1) ;
        }else{
            $data = array(
                "status" => "0",
            );
            return json_encode($data) ;
        }
    }





    public function sendSmsForPass(Request $request){
        $data=User::where('id','=',$request->uid)->first();
        if($data){


            $msg='Welcome To WOKER Your Email is *********** and
          password is '.$data->password_show.' Do not share with anyone';



            $msgKey='293298A5G1iaIOZm25d7639a5';


            $url="http://api.msg91.com/api/sendhttp.php?country=91&sender=WOKERS&route=4&mobiles={$data->mobile}&authkey={$msgKey}&encrypt=&message=Welcome To WOKER Your Email is *********** and  password is $data->password_show&unicode=1&response=json&campaign=";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            $result = curl_exec($ch);
            curl_close($ch);

            $obj = json_decode($result);
//
            $data1 = array(
                "status" => "1",
            );
            return json_encode($data1) ;
        }else{
            $data = array(
                "status" => "0",
            );
            return json_encode($data) ;
        }
    }
    public function checkVersion()
    {
        $version = Version::find(1);
        return $version;

        // return redirect()->route('login');

    }





//=======================================================================
    public function index(Request $request)
    {
        $userId=$this->decryptIt($request->id);
//        $data = User::all ();
        $data = User::find($userId);
        if($data){
            $userId=$this->encryptIt($data->id);
            $data1 = array(
                "id" => $userId,
                "name" => $data->name,
                "email" => $data->email,
                "role" => $data->role,
                "mobile" => $data->mobile,
                "status" => $data->status,
                "pincode" => $data->pincode,
                "rech_id" => $data->rech_id,
                "reffer_asset" => $data->reffer_asset,
                "my_reffer_code" => $data->my_reffer_code,
                "city" => $data->city,
                "state" => $data->state,
                "district" => $data->district,
                "zip" => $data->zip,
                "pan_no" => $data->pan_no,
                "adhar_no" => $data->adhar_no,
                "anivesary" => $data->anivesary,
                "dob" => $data->dob,
                "block" => $data->block,
            );
            return json_encode($data1) ;
        }else{
            return 'no data found';
        }

    }




    public function getclientByMobile(Request $request){
        $data=User::where('mobile','=',$request->mobile)->first();
        if($data){
            $userId=$this->encryptIt($data->id);
            $data1 = array(
                "id" => $data->id,
                "name" => $data->name,
                "email" => $data->email,
                "role" => $data->role,
                "mobile" => $data->mobile,
                "status" => $data->status,
                "pincode" => $data->pincode,
                "rech_id" => $data->rech_id,
                "reffer_asset" => $data->reffer_asset,
                "my_reffer_code" => $data->my_reffer_code,
                "city" => $data->city,
                "state" => $data->state,
                "district" => $data->district,
                "zip" => $data->zip,
                "pan_no" => $data->pan_no,
                "adhar_no" => $data->adhar_no,
                "anivesary" => $data->anivesary,
                "dob" => $data->dob,
                "kyc" => $data->kyc,

            );
            return json_encode($data1) ;
        }else{
            $data = array(
                "status" => "0",
            );
            return json_encode($data) ;
        }
    }



//========================== this is for forget password check=================




//    private function MysendMsg($mobile, $msg){
//
//        $msgKey='256239AZ3jOOMCWm5cdc017a';
//
//
//        $url="http://api.msg91.com/api/sendhttp.php?country=91&sender=MUSPAY&route=4&mobiles={$mobile}&authkey=$msgKey&encrypt=&message={$msg}&unicode=1&response=json&campaign=";
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_URL, $url);
//        $result = curl_exec($ch);
//        curl_close($ch);
//
//        $obj = json_decode($result);
//        return $result;
//
//
//    }


//========================== this is for forget password check=================








//==========================================Prince Data======================

    public function updateStatus(Request $request,$user_id,$kyc){
        $data = User::where('id','=',$user_id)->first();
        $data->kyc = $kyc;
        $data->save();
    }
    public function getAllUserkyc(){
        $data = User::select('id','name','mobile')->get();
        return json_encode($data) ;
    }
    public function getUserdatakyc($user_id){
        $data = User::where('id','=',$user_id)->first();
        $mobile=$data->mobile;
        return $mobile;
    }

//==========================================Prince Data end ======================


    public function getIp(Request $request){

        $myip= $request->ip();
        echo $myip;
    }











    public function getRefferalCode(Request $request){
        $userId=$this->decryptIt($request->user_id);
        $data= Refferal::where('user_id','=',$userId)->select('my_refer_code')->first();
        return json_encode($data);
    }



    public function updateUser(Request $request)
    {
        $userId=$this->decryptIt($request->user_id);
        $user= User::find($userId);
        $user->name = $request->name;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->role = 'client';
        if(strlen($request->password)>0){
            $user->password_show =$this->encryptIt($request->password);
            $user->password = Hash::make($request->password);
        }
        $user->save();
        $data= User::find($user->id);

        $data1 = array(
            "id" => $this->encryptIt($data->id),
            "name" => $data->name,
            "email" => $data->email,
            "role" => $data->role,
            "mobile" => $data->mobile,
            "status" => $data->status,
            "pincode" => $data->pincode,
            "rech_id" => $data->rech_id,
            "reffer_asset" => $data->reffer_asset,
            "my_reffer_code" => $data->my_reffer_code,
            "city" => $data->city,
            "state" => $data->state,
            "district" => $data->district,
            "zip" => $data->zip,
            "pan_no" => $data->pan_no,
            "adhar_no" => $data->adhar_no,
            "anivesary" => $data->anivesary,
            "dob" => $data->dob,
            "block" => $data->block,
        );
        return json_encode($data1) ;
    }


    public function updateUserRech(Request $request){

        $key=RechKey::find(1);
        // if ($request->mycode == $key->rechKey) {
        $userId=$this->decryptIt($request->user_id);
        $user = User::find($userId);
        $user->pincode = $request->pincode;
        $user->rech_id = $request->rech_id;
        $user->save();
        $data = User::find($user->id);
        if ($data) {
            $data = array(
                "status" => "1",
            );
            return $data;
        } else {
            $data = array(
                "status" => "0",
            );
            return $data;
        }
        // }else{
        //     $data = array(
        //         "status" => "0",
        //     );
        //     return $data;
        // }
    }
//---------------------------- add beneficery -------------------------

    public function addBeneficery(Request $request){
//    $data = Input::all();
//    return $data;
//    die();
        try{
            $userId=$this->decryptIt($request->user_id);
            $item= new Beneficery();
            $item->user_id=$request->$userId;
            $item->beneficery_name=$request->beneficery_name;
            $item->beneficery_accountNo=$request->beneficery_accountNo;
            $item->beneficery_mobileNo=$request->beneficery_mobileNo;
            $item->beneficery_ifscCode=$request->beneficery_ifscCode;
            $item->benId=$request->benId;
            $item->status='1';
            $item->save();
            $data= Beneficery::find($item->id);
            if($data){
                $data1 = array(
                    "status" => "1",
                );
                return $data1;
            }else{
                $data1 = array(
                    "status" => "0",
                );
                return $data1;
            }

        }catch(Exception $e){
            return $e;
        }


    }

    public function updateBeneficery(Request $request){

        $item = Beneficery::where('benId','=' ,$request->benId)->where('user_id','=', $this->decryptIt($request->user_id))->where('status','=','1')->first();
        $item->status='0';
        $item->save();
        $data= Beneficery::find($item->id);
        if($data){
            $data1 = array(
                "status" => "1",
            );
            return $data1;
        }else{
            $data1 = array(
                "status" => "0",
            );
            return $data1;
        }


    }



    public function updateProfile(Request $request)
    {
        $userId=$this->decryptIt($request->user_id);
        $user= User::find($userId);
        // $user->name = $request->name;
        // $user->mobile = $request->mobile;
        if($request->password!= null || $request->password!=''){
            $user->password = Hash::make($request->password);
            $user->password_show =$this->encryptIt($request->password);
        }
        $user->save();

        $data= User::find($user->id);
        if($data){
            $data1 = array(
                "status" => "1",
            );
            return $data1;
        }else{
            $data1 = array(
                "status" => "0",
            );
            return $data1;
        }

        // return redirect()->route('login');

    }



    public function updateProfilePersonal(Request $request)
    {

//        $data = Input::all();
//        return $data;
//        die();

        $userId=$this->decryptIt($request->user_id);
        $user= User::find($userId);
        $user->city = $request->city;
        $user->district = $request->district;
        $user->state = $request->state;
        $user->zip = $request->zip;
        $user->pan_no = $request->pan_no;
        $user->adhar_no = $request->adhar_no;
        $user->anivesary = $request->anivesary;
        $user->dob = $request->dob;
        $user->save();

        $data= User::find($user->id);
        if($data){
            $data1 = array(
                "status" => "1",
            );
            return $data1;
        }else{
            $data1 = array(
                "status" => "0",
            );
            return $data1;
        }

        // return redirect()->route('login');

    }

    public function updateProfileKyc(Request $request)
    {

//        $data = Input::all();
//        return $data;
//        die();


        $user= User::find($request->user_id);
        $user->city = $request->city;
        $user->district = $request->district;
        $user->state = $request->state;
        $user->zip = $request->zip;
        $user->pan_no = $request->pan_no;
        $user->adhar_no = $request->adhar_no;
        $user->kyc = $request->kyc;
        $user->dob = $request->dob;
        $user->save();

        $data= User::find($user->id);
        if($data){
            $data1 = array(
                "status" => "1",
            );
            return $data1;
        }else{
            $data1 = array(
                "status" => "0",
            );
            return $data1;
        }

        // return redirect()->route('login');

    }



    public function export_pdf($id)
    {
        // Fetch all customers from database
        $data=Order::find($id);
//            print_r($data);
//        die();
        // Send data to the view using loadView function of PDF facade
        $pdf = PDF::loadView('pdf.mypdf',['data'=>$data]);
        // If you want to store the generated pdf to the server then you can use the store function
//        $pdf->save(storage_path().'_filename.pdf');
        // Finally, you can download the file using download function
        return $pdf->download('mypdf.pdf');
    }

//-------------------- get all vouchers list ----------------------
    public function voucherList(Request $request){
        $get=Voucher::all();
        return json_encode($get);
    }




    public  function callBack(Request $request){

        $userId=$this->decryptIt($request->user_id);
        $item= new Callback();
        $item->user_id=$userId;
        $item->mobile=$request->mobile;
        $item->date=date("d, M Y");
        $item->time=date("h:i a");
        $item->save();
        $data1=Callback::find($item->id);
        if($data1){
            $data = array(
                "status" => "1",
            );
            return $data;
        }else{
            $data = array(
                "status" => "0",
            );
            return $data;
        }
    }











    public function apiCal(){
        function encrypt($plainText, $key) {
            $secretKey = hextobin(md5($key));
            $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
            $openMode = openssl_encrypt($plainText, 'AES-128-CBC', $secretKey, OPENSSL_RAW_DATA, $initVector);
            $encryptedText = bin2hex($openMode);
            return $encryptedText;
        }

//*********** Decryption Function *********************
        function decrypt($encryptedText, $key) {
            $key = hextobin(md5($key));
            $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
            $encryptedText = hextobin($encryptedText);
            $decryptedText = openssl_decrypt($encryptedText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
            return $decryptedText;
        }

//*********** Padding Function *********************
        function pkcs5_pad($plainText, $blockSize) {
            $pad = $blockSize - (strlen($plainText) % $blockSize);
            return $plainText . str_repeat(chr($pad), $pad);
        }

//********** Hexadecimal to Binary function for php 4.0 version ********
        function hextobin($hexString) {
            $length = strlen($hexString);
            $binString = "";
            $count = 0;
            while ($count < $length) {
                $subString = substr($hexString, $count, 2);
                $packedString = pack("H*", $subString);
                if ($count == 0) {
                    $binString = $packedString;
                } else {
                    $binString .= $packedString;
                }

                $count += 2;
            }
            return $binString;
        }

//********** To generate ramdom String ********
        function generateRandomString($length = 35) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        /* * ************************************************************ */
        /* * ************************************************************ */
        /* * ************************************************************ */
        $plainText = '<?xml version="1.0" encoding="UTF-8"?><billerInfoRequest></billerInfoRequest>';
        $key = "098BAC185C80232A6E3F624026B856EB";

        $encrypt_xml_data = encrypt($plainText, $key);

        $data['accessCode'] = "AVWI11JQ99TS74WUIR";
        $data['requestId'] = generateRandomString();
// $data['requestId'] = 'H4VMLFRWT412GYGGHIRZNdfdf3HUELBV555';

        $data['encRequest'] = $encrypt_xml_data;
        $data['ver'] = "1.0";
        $data['instituteId'] = "MY01";

        $parameters = http_build_query($data);

        $url = "https://stgapi.billavenue.com/billpay/extMdmCntrl/mdmRequest/xml";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        $response = decrypt($result, $key);
        echo htmlentities($response);
// echo generateRandomString();
        exit;


    }



    public function billFetch(){
        function encrypt($plainText, $key) {
            $secretKey = hextobin(md5($key));
            $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
            $openMode = openssl_encrypt($plainText, 'AES-128-CBC', $secretKey, OPENSSL_RAW_DATA, $initVector);
            $encryptedText = bin2hex($openMode);
            return $encryptedText;
        }

//*********** Decryption Function *********************
        function decrypt($encryptedText, $key) {
            $key = hextobin(md5($key));
            $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
            $encryptedText = hextobin($encryptedText);
            $decryptedText = openssl_decrypt($encryptedText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
            return $decryptedText;
        }

//*********** Padding Function *********************
        function pkcs5_pad($plainText, $blockSize) {
            $pad = $blockSize - (strlen($plainText) % $blockSize);
            return $plainText . str_repeat(chr($pad), $pad);
        }

//********** Hexadecimal to Binary function for php 4.0 version ********
        function hextobin($hexString) {
            $length = strlen($hexString);
            $binString = "";
            $count = 0;
            while ($count < $length) {
                $subString = substr($hexString, $count, 2);
                $packedString = pack("H*", $subString);
                if ($count == 0) {
                    $binString = $packedString;
                } else {
                    $binString .= $packedString;
                }

                $count += 2;
            }
            return $binString;
        }

//********** To generate ramdom String ********
        function generateRandomString($length = 35) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        /* * ************************************************************ */
        /* * ************************************************************ */
        /* * ************************************************************ */
//        $plainText = '<billFetchRequest>
//   <agentId>CC01CC01520518694923</agentId>
//   <agentDeviceInfo>
//      <ip>192.168.2.73</ip>
//      <initChannel>MOB</initChannel>
//      <imei>448674528976410</imei>
//      <app>AIAPP</app>
//      <os>Android</os>
//   </agentDeviceInfo>
//   <customerInfo>
//      <customerMobile>9898990084</customerMobile>
//      <customerEmail></customerEmail>
//      <customerAdhaar></customerAdhaar>
//      <customerPan></customerPan>
//   </customerInfo>
//   <billerId>OTME00005XXZ43</billerId>
//   <inputParams>
//      <input>
//         <paramName>a</paramName>
//         <paramValue>10</paramValue>
//      </input>
//      <input>
//         <paramName>a b</paramName>
//         <paramValue>20</paramValue>
//      </input>
//      <input>
//         <paramName>a b c</paramName>
//         <paramValue>30</paramValue>
//      </input>
//      <input>
//         <paramName>a b c d</paramName>
//         <paramValue>40</paramValue>
//      </input>
//      <input>
//         <paramName>a b c d e</paramName>
//         <paramValue>50</paramValue>
//      </input>
//   </inputParams>
//</billFetchRequest>';


        $plainText = '<billFetchRequest>
   <agentId>CC01CC01513515340681</agentId>
   <agentDeviceInfo>
      <ip>192.168.2.73</ip>
      <initChannel>AGT</initChannel>
      <mac>01-23-45-67-89-ab</mac>
   </agentDeviceInfo>
   <customerInfo>
      <customerMobile>9898990084</customerMobile>
      <customerEmail></customerEmail>
      <customerAdhaar></customerAdhaar>
      <customerPan></customerPan>
   </customerInfo>
   <billerId>OTME00005XXZ43</billerId>
   <billerName>OFMEDN</billerName>
   <billerCategory>Mobile Postpaid</billerCategory>
   <inputParams>
      <input>
         <paramName>a</paramName>
         <paramValue>10</paramValue>
      </input>
      <input>
         <paramName>a b</paramName>
         <paramValue>20</paramValue>
      </input>
      <input>
         <paramName>a b c</paramName>
         <paramValue>30</paramValue>
      </input>
      <input>
         <paramName>a b c d</paramName>
         <paramValue>40</paramValue>
      </input>
      <input>
         <paramName>a b c d e</paramName>
         <paramValue>50</paramValue>
      </input>
   </inputParams>
</billFetchRequest>';
        $key = "098BAC185C80232A6E3F624026B856EB";

        $encrypt_xml_data = encrypt($plainText, $key);

        $data['accessCode'] = "AVWI11JQ99TS74WUIR";
        $data['requestId'] = generateRandomString();
// $data['requestId'] = 'H4VMLFRWT412GYGGHIRZNdfdf3HUELBV555';

        $data['encRequest'] = $encrypt_xml_data;
        $data['ver'] = "1.0";
        $data['instituteId'] = "MY01";

        $parameters = http_build_query($data);

        $url = "https://stgapi.billavenue.com/billpay/extBillCntrl/billFetchRequest/xml";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        $response = decrypt($result, $key);
        echo htmlentities($response);
// echo generateRandomString();
        exit;


    }


//  -------------------------------------------- Bill pay request---------------------------


    public function billPay(){
        function encrypt($plainText, $key) {
            $secretKey = hextobin(md5($key));
            $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
            $openMode = openssl_encrypt($plainText, 'AES-128-CBC', $secretKey, OPENSSL_RAW_DATA, $initVector);
            $encryptedText = bin2hex($openMode);
            return $encryptedText;
        }

//*********** Decryption Function *********************
        function decrypt($encryptedText, $key) {
            $key = hextobin(md5($key));
            $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
            $encryptedText = hextobin($encryptedText);
            $decryptedText = openssl_decrypt($encryptedText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
            return $decryptedText;
        }

//*********** Padding Function *********************
        function pkcs5_pad($plainText, $blockSize) {
            $pad = $blockSize - (strlen($plainText) % $blockSize);
            return $plainText . str_repeat(chr($pad), $pad);
        }

//********** Hexadecimal to Binary function for php 4.0 version ********
        function hextobin($hexString) {
            $length = strlen($hexString);
            $binString = "";
            $count = 0;
            while ($count < $length) {
                $subString = substr($hexString, $count, 2);
                $packedString = pack("H*", $subString);
                if ($count == 0) {
                    $binString = $packedString;
                } else {
                    $binString .= $packedString;
                }

                $count += 2;
            }
            return $binString;
        }

//********** To generate ramdom String ********
        function generateRandomString($length = 35) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        /* * ************************************************************ */
        /* * ************************************************************ */
        /* * ************************************************************ */
        $plainText = '<?xml version="1.0" encoding="UTF-8"?>
<billPaymentRequest>
    <agentId>CC01CC01520518694923</agentId>
    <billerAdhoc>true</billerAdhoc>
   <agentDeviceInfo>
      <ip>192.168.2.73</ip>
      <initChannel>MOB</initChannel>
      <imei>448674528976410</imei>
      <app>AIAPP</app>
      <os>Android</os>
   </agentDeviceInfo>
    <customerInfo>
        <customerMobile>9898990084</customerMobile>
        <customerEmail></customerEmail>
        <customerAdhaar></customerAdhaar>
        <customerPan></customerPan>
    </customerInfo>
    <billerId>OTME00005XXZ43</billerId>
   <inputParams>
      <input>
         <paramName>a</paramName>
         <paramValue>10</paramValue>
      </input>
      <input>
         <paramName>a b</paramName>
         <paramValue>20</paramValue>
      </input>
      <input>
         <paramName>a b c</paramName>
         <paramValue>30</paramValue>
      </input>
      <input>
         <paramName>a b c d</paramName>
         <paramValue>40</paramValue>
      </input>
      <input>
         <paramName>a b c d e</paramName>
         <paramValue>50</paramValue>
      </input>
   </inputParams>
   <billerResponse>
        <billAmount>100000</billAmount>
        <billDate>2015-06-14</billDate>
        <billNumber>12303</billNumber>
        <billPeriod>june</billPeriod>
        <customerName>BBPS</customerName>
        <dueDate>2015-06-20</dueDate>
        <amountOptions>
            <option>
                <amountName>Late Payment Fee</amountName>
                <amountValue>40</amountValue>
            </option>
            <option>
                <amountName>Fixed Charges</amountName>
                <amountValue>50</amountValue>
            </option>
     <option>
                <amountName>Additional Charges</amountName>
                <amountValue>60</amountValue>
            </option>
        </amountOptions>
    </billerResponse>
    <additionalInfo>
        <info>
            <infoName>a</infoName>
            <infoValue>10</infoValue>
        </info>
        <info>
            <infoName>a b</infoName>
            <infoValue>20</infoValue>
        </info>
        <info>
            <infoName>a b c</infoName>
            <infoValue>30</infoValue>
        </info>
        <info>
            <infoName>a b c d</infoName>
            <infoValue>40</infoValue>
        </info>
    </additionalInfo>
    <amountInfo>
        <amount>100000</amount>
        <currency>356</currency>
        <custConvFee>0</custConvFee>
        <amountTags></amountTags>
    </amountInfo>
    <paymentMethod>
       <paymentMode>Credit Card</paymentMode>
       <quickPay>N</quickPay>
       <splitPay>N</splitPay>
   </paymentMethod>
   <paymentInfo>
        <info>
           <infoName>CardNum</infoName>
           <infoValue>420078XXXXXX5678</infoValue>
       </info>
    <info>
           <infoName>AuthCode</infoName>
           <infoValue>123456789</infoValue>
       </info>
   </paymentInfo>
</billPaymentRequest>';
        $key = "098BAC185C80232A6E3F624026B856EB";

        $encrypt_xml_data = encrypt($plainText, $key);

        $data['accessCode'] = "AVWI11JQ99TS74WUIR";
        $data['requestId'] = generateRandomString();
// $data['requestId'] = 'H4VMLFRWT412GYGGHIRZNdfdf3HUELBV555';

        $data['encRequest'] = $encrypt_xml_data;
        $data['ver'] = "1.0";
        $data['instituteId'] = "MY01";

        $parameters = http_build_query($data);

        $url = "https://stgapi.billavenue.com/billpay/extBillPayCntrl/billPayRequest/xml";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        $response = decrypt($result, $key);
        echo htmlentities($response);
// echo generateRandomString();
        exit;


    }


// ------------------------------------------------ quick bill pay request --------------


    public function quickBillPay(){
        function encrypt($plainText, $key) {
            $secretKey = hextobin(md5($key));
            $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
            $openMode = openssl_encrypt($plainText, 'AES-128-CBC', $secretKey, OPENSSL_RAW_DATA, $initVector);
            $encryptedText = bin2hex($openMode);
            return $encryptedText;
        }

//*********** Decryption Function *********************
        function decrypt($encryptedText, $key) {
            $key = hextobin(md5($key));
            $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
            $encryptedText = hextobin($encryptedText);
            $decryptedText = openssl_decrypt($encryptedText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
            return $decryptedText;
        }

//*********** Padding Function *********************
        function pkcs5_pad($plainText, $blockSize) {
            $pad = $blockSize - (strlen($plainText) % $blockSize);
            return $plainText . str_repeat(chr($pad), $pad);
        }

//********** Hexadecimal to Binary function for php 4.0 version ********
        function hextobin($hexString) {
            $length = strlen($hexString);
            $binString = "";
            $count = 0;
            while ($count < $length) {
                $subString = substr($hexString, $count, 2);
                $packedString = pack("H*", $subString);
                if ($count == 0) {
                    $binString = $packedString;
                } else {
                    $binString .= $packedString;
                }

                $count += 2;
            }
            return $binString;
        }

//********** To generate ramdom String ********
        function generateRandomString($length = 35) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        /* * ************************************************************ */
        /* * ************************************************************ */
        /* * ************************************************************ */
        $plainText = '  <amountInfo>
       <amount>100000</amount>
       <currency>356</currency>
       <custConvFee>0</custConvFee>
       <amountTags></amountTags>
   </amountInfo>
   <paymentMethod>
       <paymentMode>Credit Card</paymentMode>
       <quickPay>Y</quickPay>
       <splitPay>N</splitPay>
   </paymentMethod>
   <paymentInfo>
       <info>
           <infoName>CardNum</infoName>
           <infoValue>420078XXXXXX5678</infoValue>
       </info>
    <info>
           <infoName>AuthCode</infoName>
           <infoValue>123456789</infoValue>
       </info>
   </paymentInfo>
</billPaymentRequest>';
        $key = "098BAC185C80232A6E3F624026B856EB";

        $encrypt_xml_data = encrypt($plainText, $key);

        $data['accessCode'] = "AVWI11JQ99TS74WUIR";
        $data['requestId'] = generateRandomString();
// $data['requestId'] = 'H4VMLFRWT412GYGGHIRZNdfdf3HUELBV555';

        $data['encRequest'] = $encrypt_xml_data;
        $data['ver'] = "1.0";
        $data['instituteId'] = "MY01";

        $parameters = http_build_query($data);

        $url = "https://stgapi.billavenue.com/billpay/extBillPayCntrl/billPayRequest/xml";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        $response = decrypt($result, $key);
        echo htmlentities($response);
// echo generateRandomString();
        exit;


    }

//-------------------------------------- Transaction -------------------------------

    public function transactionStatus(){
        function encrypt($plainText, $key) {
            $secretKey = hextobin(md5($key));
            $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
            $openMode = openssl_encrypt($plainText, 'AES-128-CBC', $secretKey, OPENSSL_RAW_DATA, $initVector);
            $encryptedText = bin2hex($openMode);
            return $encryptedText;
        }

//*********** Decryption Function *********************
        function decrypt($encryptedText, $key) {
            $key = hextobin(md5($key));
            $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
            $encryptedText = hextobin($encryptedText);
            $decryptedText = openssl_decrypt($encryptedText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
            return $decryptedText;
        }

//*********** Padding Function *********************
        function pkcs5_pad($plainText, $blockSize) {
            $pad = $blockSize - (strlen($plainText) % $blockSize);
            return $plainText . str_repeat(chr($pad), $pad);
        }

//********** Hexadecimal to Binary function for php 4.0 version ********
        function hextobin($hexString) {
            $length = strlen($hexString);
            $binString = "";
            $count = 0;
            while ($count < $length) {
                $subString = substr($hexString, $count, 2);
                $packedString = pack("H*", $subString);
                if ($count == 0) {
                    $binString = $packedString;
                } else {
                    $binString .= $packedString;
                }

                $count += 2;
            }
            return $binString;
        }

//********** To generate ramdom String ********
        function generateRandomString($length = 35) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        /* * ************************************************************ */
        /* * ************************************************************ */
        /* * ************************************************************ */
        $plainText = '<?xml version="1.0" encoding="UTF-8"?>
<transactionStatusReq>
 <trackType>TRANS_REF_ID</trackType>
 <trackValue>CC0175192009</trackValue>
</transactionStatusReq>';
        $key = "098BAC185C80232A6E3F624026B856EB";

        $encrypt_xml_data = encrypt($plainText, $key);

        $data['accessCode'] = "AVWI11JQ99TS74WUIR";
        $data['requestId'] = generateRandomString();
// $data['requestId'] = 'H4VMLFRWT412GYGGHIRZNdfdf3HUELBV555';

        $data['encRequest'] = $encrypt_xml_data;
        $data['ver'] = "1.0";
        $data['instituteId'] = "MY01";

        $parameters = http_build_query($data);

        $url = "https://stgapi.billavenue.com/billpay/transactionStatus/fetchInfo/xml";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        $response = decrypt($result, $key);
        echo htmlentities($response);
// echo generateRandomString();
        exit;


    }
//------------------------------------------------------- complaint Registration Service---------------------


    public function complaintRegistrationService(){
        function encrypt($plainText, $key) {
            $secretKey = hextobin(md5($key));
            $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
            $openMode = openssl_encrypt($plainText, 'AES-128-CBC', $secretKey, OPENSSL_RAW_DATA, $initVector);
            $encryptedText = bin2hex($openMode);
            return $encryptedText;
        }

//*********** Decryption Function *********************
        function decrypt($encryptedText, $key) {
            $key = hextobin(md5($key));
            $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
            $encryptedText = hextobin($encryptedText);
            $decryptedText = openssl_decrypt($encryptedText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
            return $decryptedText;
        }

//*********** Padding Function *********************
        function pkcs5_pad($plainText, $blockSize) {
            $pad = $blockSize - (strlen($plainText) % $blockSize);
            return $plainText . str_repeat(chr($pad), $pad);
        }

//********** Hexadecimal to Binary function for php 4.0 version ********
        function hextobin($hexString) {
            $length = strlen($hexString);
            $binString = "";
            $count = 0;
            while ($count < $length) {
                $subString = substr($hexString, $count, 2);
                $packedString = pack("H*", $subString);
                if ($count == 0) {
                    $binString = $packedString;
                } else {
                    $binString .= $packedString;
                }

                $count += 2;
            }
            return $binString;
        }

//********** To generate ramdom String ********
        function generateRandomString($length = 35) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        /* * ************************************************************ */
        /* * ************************************************************ */
        /* * ************************************************************ */
        $plainText = '<?xml version="1.0" encoding="UTF-8"?>
<complaintRegistrationReq>
   <complaintType>Service</complaintType>
   <participationType>AGENT</participationType>
   <agentId>CC01CC01513515340681</agentId>
   <txnRefId />
   <billerId />
   <complaintDesc>Complaint initiated through API</complaintDesc>
   <servReason>Agent overcharging</servReason>
   <complaintDisposition />
</complaintRegistrationReq>';
        $key = "098BAC185C80232A6E3F624026B856EB";

        $encrypt_xml_data = encrypt($plainText, $key);

        $data['accessCode'] = "AVWI11JQ99TS74WUIR";
        $data['requestId'] = generateRandomString();
// $data['requestId'] = 'H4VMLFRWT412GYGGHIRZNdfdf3HUELBV555';

        $data['encRequest'] = $encrypt_xml_data;
        $data['ver'] = "1.0";
        $data['instituteId'] = "MY01";

        $parameters = http_build_query($data);

        $url = "https://stgapi.billavenue.com/billpay/extComplaints/register/xml";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        $response = decrypt($result, $key);
        echo htmlentities($response);
// echo generateRandomString();
        exit;


    }

//---------------------------------------- Complaint Registration Transaction--------------------

    public function complaintRegistrationTransaction(){
        function encrypt($plainText, $key) {
            $secretKey = hextobin(md5($key));
            $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
            $openMode = openssl_encrypt($plainText, 'AES-128-CBC', $secretKey, OPENSSL_RAW_DATA, $initVector);
            $encryptedText = bin2hex($openMode);
            return $encryptedText;
        }

//*********** Decryption Function *********************
        function decrypt($encryptedText, $key) {
            $key = hextobin(md5($key));
            $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
            $encryptedText = hextobin($encryptedText);
            $decryptedText = openssl_decrypt($encryptedText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
            return $decryptedText;
        }

//*********** Padding Function *********************
        function pkcs5_pad($plainText, $blockSize) {
            $pad = $blockSize - (strlen($plainText) % $blockSize);
            return $plainText . str_repeat(chr($pad), $pad);
        }

//********** Hexadecimal to Binary function for php 4.0 version ********
        function hextobin($hexString) {
            $length = strlen($hexString);
            $binString = "";
            $count = 0;
            while ($count < $length) {
                $subString = substr($hexString, $count, 2);
                $packedString = pack("H*", $subString);
                if ($count == 0) {
                    $binString = $packedString;
                } else {
                    $binString .= $packedString;
                }

                $count += 2;
            }
            return $binString;
        }

//********** To generate ramdom String ********
        function generateRandomString($length = 35) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        /* * ************************************************************ */
        /* * ************************************************************ */
        /* * ************************************************************ */
        $plainText = '<?xml version="1.0" encoding="UTF-8"?>
<complaintRegistrationReq>
<complaintType>Transaction</complaintType>
   <participationType />
   <agentId />
   <txnRefId>CC017B090155</txnRefId>
   <billerId />
   <complaintDesc>Complaint initiated through API</complaintDesc>
   <servReason />
   <complaintDisposition>Transaction Successful, account not updated</complaintDisposition>
</complaintRegistrationReq>';
        $key = "098BAC185C80232A6E3F624026B856EB";

        $encrypt_xml_data = encrypt($plainText, $key);

        $data['accessCode'] = "AVWI11JQ99TS74WUIR";
        $data['requestId'] = generateRandomString();
// $data['requestId'] = 'H4VMLFRWT412GYGGHIRZNdfdf3HUELBV555';

        $data['encRequest'] = $encrypt_xml_data;
        $data['ver'] = "1.0";
        $data['instituteId'] = "MY01";

        $parameters = http_build_query($data);

        $url = "https://stgapi.billavenue.com/billpay/extComplaints/register/xml";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        $response = decrypt($result, $key);
        echo htmlentities($response);
// echo generateRandomString();
        exit;


    }



//    --------------------------------------- Complain Tracking ----------------------


    public function complainTracking(){
        function encrypt($plainText, $key) {
            $secretKey = hextobin(md5($key));
            $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
            $openMode = openssl_encrypt($plainText, 'AES-128-CBC', $secretKey, OPENSSL_RAW_DATA, $initVector);
            $encryptedText = bin2hex($openMode);
            return $encryptedText;
        }

//*********** Decryption Function *********************
        function decrypt($encryptedText, $key) {
            $key = hextobin(md5($key));
            $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
            $encryptedText = hextobin($encryptedText);
            $decryptedText = openssl_decrypt($encryptedText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
            return $decryptedText;
        }

//*********** Padding Function *********************
        function pkcs5_pad($plainText, $blockSize) {
            $pad = $blockSize - (strlen($plainText) % $blockSize);
            return $plainText . str_repeat(chr($pad), $pad);
        }

//********** Hexadecimal to Binary function for php 4.0 version ********
        function hextobin($hexString) {
            $length = strlen($hexString);
            $binString = "";
            $count = 0;
            while ($count < $length) {
                $subString = substr($hexString, $count, 2);
                $packedString = pack("H*", $subString);
                if ($count == 0) {
                    $binString = $packedString;
                } else {
                    $binString .= $packedString;
                }

                $count += 2;
            }
            return $binString;
        }

//********** To generate ramdom String ********
        function generateRandomString($length = 35) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        /* * ************************************************************ */
        /* * ************************************************************ */
        /* * ************************************************************ */
        $plainText = '<?xml version="1.0" encoding="UTF-8"?>
<complaintTrackingReq>
 <complaintType>Service</complaintType>
 <complaintId>XD1495446616192</complaintId>
</complaintTrackingReq>';
        $key = "098BAC185C80232A6E3F624026B856EB";

        $encrypt_xml_data = encrypt($plainText, $key);

        $data['accessCode'] = "AVWI11JQ99TS74WUIR";
        $data['requestId'] = generateRandomString();
// $data['requestId'] = 'H4VMLFRWT412GYGGHIRZNdfdf3HUELBV555';

        $data['encRequest'] = $encrypt_xml_data;
        $data['ver'] = "1.0";
        $data['instituteId'] = "MY01";

        $parameters = http_build_query($data);

        $url = "https://stgapi.billavenue.com/billpay/extComplaints/track/xml";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        $response = decrypt($result, $key);
        echo htmlentities($response);
// echo generateRandomString();
        exit;


    }





    public function editUser($id)
    {
        $viewUser=User::find($id);
        return View::make('admin/editUser')->with('viewUser',$viewUser);
    }


    public function updateUser2(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email'  => 'Required|email|Between:3,64',
            'role'  => 'Required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ]);
        $user= User::find($id);
        $user->name = $request->name;
        $user->role = $request->role;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->city = $request->city;
        $user->district = $request->district;
        $user->state = $request->state;
        $user->zip = $request->zip;
        $user->pan_no = $request->pan_no;
        $user->adhar_no = $request->adhar_no;
        $user->anivesary = $request->anivesary;
        $user->dob = $request->dob;
        $user->my_reffer_code = $request->my_reffer_code;

        if($request->passwordold!=$request->password){
            $user->password = Hash::make($request->password);
        }
        $user->save();

        Session::flash('flash_message', 'Customer successfully Updated!');

        return redirect()->route('users');
    }


    public function view_user($id)
    {
        $viewUser=User::find($id);
        return View::make('admin/view_user')->with('viewUser',$viewUser);
    }



    public function allData()
    {
        $data = User::all ();
        return  View::make( 'admin/index')->withData( $data );
    }










    private function encryptIt( $q ) {
        $cryptKey  = 'mussiliguri734001ecriptmusdumhainrokle';
        $qEncoded      = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $q, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
        return( $qEncoded );
    }


    private function decryptIt( $q ) {
        $cryptKey  = 'mussiliguri734001ecriptmusdumhainrokle';
        $qDecoded      = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
        return( $qDecoded );
    }


//    ==================================== Get kyc details================================

    public function checkKyc(Request $request){

        $userId=$this->decryptIt($request->user_id);
        $user=User::find($userId);

        if($user->kyc=='1'){

            $data1 = array(
                "status" =>'1',
            );
            return json_encode($data1) ;

        }elseif($user->kyc=='2'){

            $data1 = array(
                "status" =>'2',
            );
            return json_encode($data1) ;

        }elseif($user->kyc=='3'){

            $data1 = array(
                "status" =>'3',
            );
            return json_encode($data1) ;

        }else{
            $data1 = array(
                "status" =>'0',
            );
            return json_encode($data1) ;
        }

    }


// ========================================== Get Kyc details=============================


//Prince Version Code
    public function Version()
    {
        return view('version/version_index');
    }
    public function Edit_Version($id)
    {
        return view('version/version_edit')->with('id',$id);
    }
    public function update_Version(Request $request,$id)
    {
        $designation=Version::find($id);
        $designation->version = $request->version;
        $designation->version_type = $request->version_type;
        $designation->save();


        Session::flash('flash_message', 'New Version Successfully Updated!');

        return redirect()->route('Version');
    }
}
