<?php

namespace App\Http\Controllers;

use App\Woker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class WorkerAdminController extends Controller
{
    public function Worker()
    {
        return view('workers/worker_index');
    }
    public function Add_Worker()
    {
        return view('workers/worker_add');
    }
    public function Edit_Worker($id)
    {
        return view('workers/worker_edit')->with('id',$id);
    }
    public function store_worker(Request $request)
    {
        $aadhar = $request->aadhar_document;
        $pan = $request->pan_document;
        $voter = $request->voter_id_document;
        if(Input::hasFile('aadhar_document')) {
            $filename = time() . $aadhar->getClientOriginalName();
            $aadhar->move('public/uploads/worker_documents', $filename);
        }elseif(Input::hasFile('pan_document')) {
            $filename1 = time() . $pan->getClientOriginalName();
            $pan->move('public/uploads/worker_documents', $filename1);
        }elseif(Input::hasFile('voter_id_document')) {
            $filename2 = time() . $voter->getClientOriginalName();
            $voter->move('public/uploads/worker_documents', $filename2);
        }

        $worker=new Woker();
        $worker->woker_name=$request->worker_name;
        $worker->woker_add=$request->woker_add;
        $worker->street_one=$request->street_one;
        $worker->street_two=$request->street_two;
        $worker->city=$request->city;
        $worker->email=$request->email;
        $worker->password=$request->password;
        $worker->mobile=$request->mobile_no;
        $worker->adhar_no=$request->aadhar_no;
        $worker->pan_no=$request->pan_no;
        $worker->voter_id=$request->voter_id;
        $worker->workDetails=$request->worker_name;
        $worker->zip=$request->zip;
        $worker->block=$request->block;
        $worker->checkStatus=$request->status;
        $worker->designation_id=$request->worker_designation;
        $designations=DB::table('woker_desig')->find($request->worker_designation);
        $worker->designation_name=$designations->job_name;

        if(Input::hasFile('aadhar_document')) {
            $worker->aadhar_document=$filename;
        }elseif(Input::hasFile('pan_document')) {
            $worker->pan_document=$filename1;
        }elseif(Input::hasFile('voter_id_document')) {
            $worker->voter_document=$filename2;
        }
        $worker->save();

        Session::flash('flash_message', 'Worker Successfully Added!');

        return redirect()->route('Worker');
    }
    public function update_worker(Request $request,$id)
    {
        $aadhar = $request->aadhar_document;
        $pan = $request->pan_document;
        $voter = $request->voter_id_document;
        if(Input::hasFile('aadhar_document')) {
            $filename = time() . $aadhar->getClientOriginalName();
            $aadhar->move('public/uploads/worker_documents', $filename);
        }elseif(Input::hasFile('pan_document')) {
            $filename1 = time() . $pan->getClientOriginalName();
            $pan->move('public/uploads/worker_documents', $filename1);
        }elseif(Input::hasFile('voter_id_document')) {
            $filename2 = time() . $voter->getClientOriginalName();
            $voter->move('public/uploads/worker_documents', $filename2);
        }

        $worker=Woker::find($id);
        $worker->woker_name=$request->worker_name;
        $worker->woker_add=$request->woker_add;
        $worker->street_one=$request->street_one;
        $worker->street_two=$request->street_two;
        $worker->city=$request->city;
        $worker->email=$request->email;
        $worker->password=$request->password;
        $worker->mobile=$request->mobile_no;
        $worker->adhar_no=$request->aadhar_no;
        $worker->pan_no=$request->pan_no;
        $worker->voter_id=$request->voter_id;
        $worker->workDetails=$request->worker_name;
        $worker->zip=$request->zip;
        $worker->block=$request->block;
        $worker->checkStatus=$request->status;
        $worker->designation_id=$request->worker_designation;
        $designations=DB::table('woker_desig')->find($request->worker_designation);
        $worker->designation_name=$designations->job_name;

        if(Input::hasFile('aadhar_document')) {
            $worker->aadhar_document=$filename;
        }elseif(Input::hasFile('pan_document')) {
            $worker->pan_document=$filename1;
        }elseif(Input::hasFile('voter_id_document')) {
            $worker->voter_document=$filename2;
        }
        $worker->save();

        Session::flash('flash_message', 'Worker Successfully Updated!');

        return redirect()->route('Worker');
    }
}
