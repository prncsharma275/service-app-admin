<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Booking extends Authenticatable
{
    use Notifiable;

    protected $table = 'booking';

    protected $fillable=array('id',);

}
